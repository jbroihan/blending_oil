class CustomersController < ApplicationController
  load_and_authorize_resource
  before_action :set_customer, only: [:show, :edit, :update, :destroy]

  # GET /customers
  # GET /customers.json
  def index
    if can? :optimize, Warehousing
      @customers = Customer.all
    else
      @customers = Customer.where(customers: {namefi: current_user.firstname, namela: current_user.name})
    end

    respond_to do |format|
      format.html
      format.xls { send_data(@customers.to_xls) }
      format.xls {
        filename = "Customers-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@customers.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
    end
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Kunde wurde erfolgreich erstellt.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: 'Kunde wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Kunde wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end

  def be
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_customer
    @customer = Customer.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def customer_params
    params.require(:customer).permit(:namefi, :namela, :email, :gams_id, :adress,
                                     orders_attributes: [:id, :_destroy, :month_id, :final_product_id, :customer_id, :quantity])
  end
end
