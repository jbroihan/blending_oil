# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('#orders').dataTable({
    "language": {
      "lengthMenu": "Einträge pro Seite: _MENU_",
      "sSearch": "Suche:",
      "oPaginate": {
        "sPrevious": "vorherige Seite"
        "sNext": "nächste Seite"
      }
    }
    "dom": '<"top"i>rt<"bottom"flp><"clear">',
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
  })






