json.array!(@warehousings) do |warehousing|
  json.extract! warehousing, :id, :rawoil_id, :storage_id, :month_id, :stock
  json.url warehousing_url(warehousing, format: :json)
end
