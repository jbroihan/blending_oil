require 'test_helper'

class ProductPeriodAssociationsControllerTest < ActionController::TestCase
  setup do
    @product_period_association = product_period_associations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_period_associations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_period_association" do
    assert_difference('ProductPeriodAssociation.count') do
      post :create, product_period_association: { demand: @product_period_association.demand, inventory_level: @product_period_association.inventory_level, period_id: @product_period_association.period_id, product_id: @product_period_association.product_id, production_quantity: @product_period_association.production_quantity, setup_variable: @product_period_association.setup_variable }
    end

    assert_redirected_to product_period_association_path(assigns(:product_period_association))
  end

  test "should show product_period_association" do
    get :show, id: @product_period_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_period_association
    assert_response :success
  end

  test "should update product_period_association" do
    patch :update, id: @product_period_association, product_period_association: { demand: @product_period_association.demand, inventory_level: @product_period_association.inventory_level, period_id: @product_period_association.period_id, product_id: @product_period_association.product_id, production_quantity: @product_period_association.production_quantity, setup_variable: @product_period_association.setup_variable }
    assert_redirected_to product_period_association_path(assigns(:product_period_association))
  end

  test "should destroy product_period_association" do
    assert_difference('ProductPeriodAssociation.count', -1) do
      delete :destroy, id: @product_period_association
    end

    assert_redirected_to product_period_associations_path
  end
end
