json.array!(@orders) do |order|
  json.extract! order, :id, :customer_id, :final_product_id, :month_id, :quantity
  json.url order_url(order, format: :json)
end
