json.array!(@administrations) do |administration|
  json.extract! administration, :id, :category
  json.url administration_url(administration, format: :json)
end
