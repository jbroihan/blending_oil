# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( customers1.png )
Rails.application.config.assets.precompile += %w( Lieferant.png )
Rails.application.config.assets.precompile += %w( month.png )
Rails.application.config.assets.precompile += %w( rawoil.png )
Rails.application.config.assets.precompile += %w( Raffinerie.png )
Rails.application.config.assets.precompile += %w( Lagertank.png )
Rails.application.config.assets.precompile += %w( Endprodukt.png )
Rails.application.config.assets.precompile += %w( produktionsparameter.png )
Rails.application.config.assets.precompile += %w( shipment.png )
Rails.application.config.assets.precompile += %w( Warehouse_With_Boxes-512.png )
Rails.application.config.assets.precompile += %w( shopping_cart-512.png )
Rails.application.config.assets.precompile += %w( rohoelverbrauch.png )