class FinalProduct < ActiveRecord::Base
  validates :price, :numericality => { :greater_than => 0}

  has_many :rawoils # 1:n zwischen Endprodukt und Rohöhlen

  # n:m zwischen Endprodukten, Kunden und Monaten
  has_many :orders #Verbindungstabelle
  has_many :customers, :through => :orders
  has_many :months, :through => :orders
end
