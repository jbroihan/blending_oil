class CreateConsumptions < ActiveRecord::Migration
  def change
    create_table :consumptions do |t|
      t.integer :month_id
      t.integer :rawoil_id
      t.binary :induse
      t.float :use

      t.timestamps null: false
    end
  end
end
