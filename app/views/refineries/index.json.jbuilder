json.array!(@refineries) do |refinery|
  json.extract! refinery, :id, :name, :capacity, :category
  json.url refinery_url(refinery, format: :json)
end
