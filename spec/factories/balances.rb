require 'faker'

FactoryGirl.define do
  factory :balance do
    value { Faker::Number.number(5) }
  end
end