class Shipment < ActiveRecord::Base
  validates :price, presence: true, :numericality => { :greater_than => 0}
  validates_uniqueness_of :month_id, scope: [ :rawoil_id, :supplier_id ]

  belongs_to :supplier
  belongs_to :rawoil
  belongs_to :month

end
