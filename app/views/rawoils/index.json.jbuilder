json.array!(@rawoils) do |rawoil|
  json.extract! rawoil, :id, :name, :density, :category
  json.url rawoil_url(rawoil, format: :json)
end
