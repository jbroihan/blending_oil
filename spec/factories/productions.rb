require 'faker'

FactoryGirl.define do
  factory :production do
    month_id    { Faker::Number.between(1, 6) }
    production  { Faker::Number.positive }
  end
end