class ProductProductAssociation < ActiveRecord::Base
  belongs_to :successor, class_name: "Product"
  belongs_to :predecessor, class_name: "Product"
  validates :successor_id, presence: true
  validates :predecessor_id, presence: true
end

