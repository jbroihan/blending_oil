class ShipmentsController < ApplicationController
  load_and_authorize_resource
  before_action :set_shipment, only: [:show, :edit, :update, :destroy]

  # GET /shipments
  # GET /shipments.json
  def index
    # If current_user is admin, index all orders
    if can? :optimize, Warehousing
      @shipments = Shipment.all
      @supplier = Supplier.all
    else
      # If current_user is not admin, index only those matching current_users name
      @shipments = Shipment.joins(:supplier).where(suppliers: {contact_firstname: current_user.firstname,
                                                                contact_lastname: current_user.name})
      @supplier = Supplier.where(suppliers: {contact_firstname: current_user.firstname,
                                             contact_lastname: current_user.name})
    end

    respond_to do |format|
      format.html
      format.xls { send_data(@shipments.to_xls) }
      format.xls {
        filename = "Shipments-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@shipments.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /shipments/1
  # GET /shipments/1.json
  def show
  end

  # GET /shipments/new
  def new
    @shipment = Shipment.new
    @supplier = Supplier.where(suppliers: {contact_firstname: current_user.firstname,
                                           contact_lastname: current_user.name})
  end

  # GET /shipments/1/edit
  def edit
      @supplier = Supplier.where(suppliers: {contact_firstname: current_user.firstname, contact_lastname: current_user.name})
  end

  # POST /shipments
  # POST /shipments.json
  def create
    @shipment = Shipment.new(shipment_params)

    respond_to do |format|
      if @shipment.save
        format.html { redirect_to @shipment, notice: 'Rohöllieferung wurde erfolgreich erstellt.' }
        format.json { render :show, status: :created, location: @shipment }
      else
        format.html { render :new }
        format.json { render json: @shipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shipments/1
  # PATCH/PUT /shipments/1.json
  def update
    respond_to do |format|
      if @shipment.update(shipment_params)
        format.html { redirect_to @shipment, notice: 'Rohöllieferung wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @shipment }
      else
        format.html { render :edit }
        format.json { render json: @shipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shipments/1
  # DELETE /shipments/1.json
  def destroy
    @shipment.destroy
    respond_to do |format|
      format.html { redirect_to shipments_url, notice: 'Rohöllieferung wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shipment
      @shipment = Shipment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shipment_params
      params.require(:shipment).permit(:supplier_id, :rawoil_id, :month_id, :quantity, :price)
    end
end
