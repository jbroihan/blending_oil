class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :namefi
      t.string :namela
      t.string :email

      t.timestamps null: false
    end
  end
end
