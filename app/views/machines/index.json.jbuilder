json.array!(@machines) do |machine|
  json.extract! machine, :id, :name, :overtime_cost
  json.url machine_url(machine, format: :json)
end
