class MonthsController < ApplicationController
  load_and_authorize_resource
  before_action :set_month, only: [:show, :edit, :update]

  # GET /months
  # GET /months.json
  def index
    @months = Month.all

    respond_to do |format|
      format.html
      format.xls { send_data(@months.to_xls) }
      format.xls {
        filename = "Months-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@months.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /months/1
  # GET /months/1.json
  def show
  end

  # GET /months/1/edit
  def edit
  end

  # PATCH/PUT /months/1
  # PATCH/PUT /months/1.json
  def update
    respond_to do |format|
      if @month.update(month_params)
        format.html { redirect_to @month, notice: 'Monat wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @month }
      else
        format.html { render :edit }
        format.json { render json: @month.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_month
    @month = Month.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def month_params
    params.require(:month).permit(:name, :description)
  end
end
