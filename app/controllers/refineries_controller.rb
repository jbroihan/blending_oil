class RefineriesController < ApplicationController
  load_and_authorize_resource
  before_action :set_refinery, only: [:show, :edit, :update]

  # GET /refineries
  # GET /refineries.json
  def index
    @refineries = Refinery.all

    respond_to do |format|
      format.html
      format.xls { send_data(@refineries.to_xls) }
      format.xls {
        filename = "Refineries-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@refineries.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /refineries/1
  # GET /refineries/1.json
  def show
  end

  # GET /refineries/1/edit
  def edit
  end

  # PATCH/PUT /refineries/1
  # PATCH/PUT /refineries/1.json
  def update
    respond_to do |format|
      if @refinery.update(refinery_params)
        format.html { redirect_to @refinery, notice: 'Raffinerie wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @refinery }
      else
        format.html { render :edit }
        format.json { render json: @refinery.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_refinery
    @refinery = Refinery.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def refinery_params
    params.require(:refinery).permit(:name, :capacity, :type)
  end
end
