require 'rails_helper'

describe 'suppliers/show.html.erb' do
  before do
    assign(:supplier, create(:supplier, name: 'PeterPanGmbH',
                             email: 'peter@pan.de',
                             contact_firstname: 'Peter',
                             contact_lastname: 'Pan'))
    render
  end
  it 'displays suppliers information correctly' do
    expect(rendered).to match /Peter/
    expect(rendered).to match /Pan/
    expect(rendered).to match /PeterPanGmbH/
    expect(rendered).to match /peter@pan.de/
  end

  it 'renders partial "table"' do
    expect(rendered).to render_template(:partial => '_table')
  end

  it 'displays an edit and back link' do
    expect(rendered).to match /Bearbeiten/
    expect(rendered).to match /Übersicht der Lieferanten/
  end
end