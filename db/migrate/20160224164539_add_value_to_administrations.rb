class AddValueToAdministrations < ActiveRecord::Migration
  def change
    add_column :administrations, :value, :float
  end
end
