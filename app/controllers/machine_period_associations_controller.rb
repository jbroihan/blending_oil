class MachinePeriodAssociationsController < ApplicationController
  before_action :set_machine_period_association, only: [:show, :edit, :update, :destroy]

  # GET /machine_period_associations
  # GET /machine_period_associations.json
  def index
    @machine_period_associations = MachinePeriodAssociation.all
  end

  # GET /machine_period_associations/1
  # GET /machine_period_associations/1.json
  def show
  end

  # GET /machine_period_associations/new
  def new
    @machine_period_association = MachinePeriodAssociation.new
  end

  # GET /machine_period_associations/1/edit
  def edit
  end

  # POST /machine_period_associations
  # POST /machine_period_associations.json
  def create
    @machine_period_association = MachinePeriodAssociation.new(machine_period_association_params)

    respond_to do |format|
      if @machine_period_association.save
        format.html { redirect_to @machine_period_association, notice: 'Machine period association was successfully created.' }
        format.json { render :show, status: :created, location: @machine_period_association }
      else
        format.html { render :new }
        format.json { render json: @machine_period_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /machine_period_associations/1
  # PATCH/PUT /machine_period_associations/1.json
  def update
    respond_to do |format|
      if @machine_period_association.update(machine_period_association_params)
        format.html { redirect_to @machine_period_association, notice: 'Machine period association was successfully updated.' }
        format.json { render :show, status: :ok, location: @machine_period_association }
      else
        format.html { render :edit }
        format.json { render json: @machine_period_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /machine_period_associations/1
  # DELETE /machine_period_associations/1.json
  def destroy
    @machine_period_association.destroy
    respond_to do |format|
      format.html { redirect_to machine_period_associations_url, notice: 'Machine period association was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_machine_period_association
      @machine_period_association = MachinePeriodAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def machine_period_association_params
      params.require(:machine_period_association).permit(:machine_id, :period_id, :capacity, :overtime)
    end
end
