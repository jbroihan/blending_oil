require 'test_helper'

class WarehousingsControllerTest < ActionController::TestCase
  setup do
    @warehousing = warehousings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:warehousings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create warehousing" do
    assert_difference('Warehousing.count') do
      post :create, warehousing: { month_id: @warehousing.month_id, rawoil_id: @warehousing.rawoil_id, stock: @warehousing.stock, storage_id: @warehousing.storage_id }
    end

    assert_redirected_to warehousing_path(assigns(:warehousing))
  end

  test "should show warehousing" do
    get :show, id: @warehousing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @warehousing
    assert_response :success
  end

  test "should update warehousing" do
    patch :update, id: @warehousing, warehousing: { month_id: @warehousing.month_id, rawoil_id: @warehousing.rawoil_id, stock: @warehousing.stock, storage_id: @warehousing.storage_id }
    assert_redirected_to warehousing_path(assigns(:warehousing))
  end

  test "should destroy warehousing" do
    assert_difference('Warehousing.count', -1) do
      delete :destroy, id: @warehousing
    end

    assert_redirected_to warehousings_path
  end
end
