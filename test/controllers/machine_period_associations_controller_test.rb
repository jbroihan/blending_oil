require 'test_helper'

class MachinePeriodAssociationsControllerTest < ActionController::TestCase
  setup do
    @machine_period_association = machine_period_associations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:machine_period_associations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create machine_period_association" do
    assert_difference('MachinePeriodAssociation.count') do
      post :create, machine_period_association: { capacity: @machine_period_association.capacity, machine_id: @machine_period_association.machine_id, overtime: @machine_period_association.overtime, period_id: @machine_period_association.period_id }
    end

    assert_redirected_to machine_period_association_path(assigns(:machine_period_association))
  end

  test "should show machine_period_association" do
    get :show, id: @machine_period_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @machine_period_association
    assert_response :success
  end

  test "should update machine_period_association" do
    patch :update, id: @machine_period_association, machine_period_association: { capacity: @machine_period_association.capacity, machine_id: @machine_period_association.machine_id, overtime: @machine_period_association.overtime, period_id: @machine_period_association.period_id }
    assert_redirected_to machine_period_association_path(assigns(:machine_period_association))
  end

  test "should destroy machine_period_association" do
    assert_difference('MachinePeriodAssociation.count', -1) do
      delete :destroy, id: @machine_period_association
    end

    assert_redirected_to machine_period_associations_path
  end
end
