require 'faker'

FactoryGirl.define do
  factory :user do
    name      { Faker::Name.last_name }
    firstname { Faker::Name.first_name }
    email     { Faker::Internet.email }
    password  { Faker::Internet.password }
  end
end


