class CreateMachinePeriodAssociations < ActiveRecord::Migration
  def change
    create_table :machine_period_associations do |t|
      t.integer :machine_id
      t.integer :period_id
      t.float :capacity
      t.float :overtime

      t.timestamps null: false
    end
  end
end
