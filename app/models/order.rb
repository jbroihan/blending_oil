class Order < ActiveRecord::Base
  validates :month_id, :final_product_id, :customer_id, :quantity, presence: true
  validates :month_id, uniqueness: { scope: :customer_id }
  validates :quantity, numericality: { greater_than_or_equal_to: 1 }

  belongs_to :customer
  belongs_to :final_product
  belongs_to :month

end
