class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @customers = Customer.where(customers: {namefi: current_user.firstname, namela: current_user.name})
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
