class CreateRawoils < ActiveRecord::Migration
  def change
    create_table :rawoils do |t|
      t.string :name
      t.float :density
      t.string :category

      t.timestamps null: false
    end
  end
end
