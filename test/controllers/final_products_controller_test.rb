require 'test_helper'

class FinalProductsControllerTest < ActionController::TestCase
  setup do
    @final_product = final_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:final_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create final_product" do
    assert_difference('FinalProduct.count') do
      post :create, final_product: { name: @final_product.name, price: @final_product.price }
    end

    assert_redirected_to final_product_path(assigns(:final_product))
  end

  test "should show final_product" do
    get :show, id: @final_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @final_product
    assert_response :success
  end

  test "should update final_product" do
    patch :update, id: @final_product, final_product: { name: @final_product.name, price: @final_product.price }
    assert_redirected_to final_product_path(assigns(:final_product))
  end

  test "should destroy final_product" do
    assert_difference('FinalProduct.count', -1) do
      delete :destroy, id: @final_product
    end

    assert_redirected_to final_products_path
  end
end
