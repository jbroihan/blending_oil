class AddDescriptionToStorage < ActiveRecord::Migration
  def change
    add_column :storages, :description, :string
  end
end
