class AdministrationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_administration, only: [:show, :edit]
  before_action :set_full_administration, only: :update

  # GET /administrations
  # GET /administrations.json
  def index
    @administrations = Administration.all

    respond_to do |format|
      format.html
      format.xls { send_data(@administrations.to_xls) }
      format.xls {
        filename = "Administrations-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@administrations.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /administrations/1
  # GET /administrations/1.json
  def show
  end

  # GET /administrations/1/edit
  def edit
  end

  # PATCH/PUT /administrations/1
  # PATCH/PUT /administrations/1.json
  def update
    respond_to do |format|
      if @administration.update(administration_params)
        format.html { redirect_to @administration, notice: 'Parameter wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @administration }

        # Storage capacity
        if @administration == @storage_capacity
          Storage.all.each do |storage|
            storage.capacity = @storage_capacity.value
            storage.save
          end
          if @storage_capacity.value < @start_end_stock.value
            @start_end_stock.value = @storage_capacity.value
            @start_end_stock.save
            flash.alert = 'Lagerkapazität aller Lagertanks sowie Anfangs- und Endlagerbestand aktualisiert.'
          else
            flash.alert = 'Lagerkapazität aller Lagertanks aktualisiert.'
          end
        end

        # Start-End-Stock
        if @administration == @start_end_stock
          if @start_end_stock.value > @storage_capacity.value
            @storage_capacity.value = @start_end_stock.value
            @storage_capacity.save
            Storage.all.each do |storage|
              storage.capacity = @storage_capacity.value
              storage.save
            end
            flash.alert = 'Anfangs- und Endlagerbestand sowie maximale Lagerkapazität aktualisiert.'
          else
            flash.alert = 'Anfangs- und Endlagerbestand aktualisiert.'
          end
        end

      else
        format.html { render :edit }
        format.json { render json: @administration.errors, status: :unprocessable_entity }
      end
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_administration
    @administration = Administration.find(params[:id])
  end

  def set_full_administration
    @administration = Administration.find(params[:id])
    @storage_capacity = Administration.find(3)
    @start_end_stock = Administration.find(4)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def administration_params
    params.require(:administration).permit(:category, :value)
  end
end
