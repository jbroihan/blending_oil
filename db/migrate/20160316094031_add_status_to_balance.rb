class AddStatusToBalance < ActiveRecord::Migration
  def change
    add_column :balances, :status, :float
  end
end
