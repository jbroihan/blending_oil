require 'faker'

FactoryGirl.define do
  factory :month do
    name          { 'm' + Faker::Number.number(1) }
    description   'Januar'
  end
end