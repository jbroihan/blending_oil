class FinalProductsController < ApplicationController
  load_and_authorize_resource
  before_action :set_final_product, only: [:show, :edit, :update]

  # GET /final_products
  # GET /final_products.json
  def index
    @final_products = FinalProduct.all

    respond_to do |format|
      format.html
      format.xls { send_data(@final_products.to_xls) }
      format.xls {
        filename = "Final-products-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@final_products.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /final_products/1
  # GET /final_products/1.json
  def show
  end

  # GET /final_products/1/edit
  def edit
  end

  # PATCH/PUT /final_products/1
  # PATCH/PUT /final_products/1.json
  def update
    respond_to do |format|
      if @final_product.update(final_product_params)
        format.html { redirect_to @final_product, notice: 'Endprodukt wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @final_product }
      else
        format.html { render :edit }
        format.json { render json: @final_product.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_final_product
    @final_product = FinalProduct.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def final_product_params
    params.require(:final_product).permit(:name, :price)
  end
end
