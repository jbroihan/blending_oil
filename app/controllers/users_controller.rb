class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
    @roles = Role.where.not(id: 1)
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      # Create Customer
      if @user.role_ids.include?(2)
        @customer = Customer.new
        flash[:success] = "Willkommen!"
        redirect_to customer_signup_path
        # Create Supplier
      elsif @user.role_ids.include?(3)
        @customer = Customer.new
        flash[:success] = "Willkommen!"
        redirect_to supplier_signup_path
      elsif @user.role_ids.include?(4)
        # Guestrole
        @customer = Customer.new
        flash[:success] = "Willkommen! Ihre Rechte werden zunächst durch einen Administrator geprüft."
        redirect_to root_path
      end
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
    if can? :optimize, Warehousing
      @roles = Role.all
    else
      @roles = Role.where.not(id: 1)
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profil wurde aktualisiert"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Benutzer wurde gelöscht"
    redirect_to users_url
  end


  private

  def user_params
    params.require(:user).permit(:name, :firstname, :email, :password,
                                 :password_confirmation, role_ids: [])
  end

  # Before filters

  # Confirms the correct user.

  def correct_user
    if can? :optimize, Warehousing
      @user = User.find(params[:id])
    else
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
  end

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
