require 'faker'

FactoryGirl.define do
  factory :administration do
    category  { Faker::Lorem.words(1) }
    value     { Faker::Number.number(3) }
  end
end