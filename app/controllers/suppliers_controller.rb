class SuppliersController < ApplicationController
  load_and_authorize_resource
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]

  # GET /suppliers
  # GET /suppliers.json
  def index
    # If current_user is admin, index all orders
    if can? :optimize, Warehousing
      @suppliers = Supplier.all
    else
      # If current_user is not admin, index only those matching current_users name
      @suppliers = Supplier.where(suppliers: {contact_firstname: current_user.firstname,
                                              contact_lastname: current_user.name})
    end

    respond_to do |format|
      format.html
      format.xls { send_data(@suppliers.to_xls) }
      format.xls {
        filename = "Suppliers-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@suppliers.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /suppliers/1
  # GET /suppliers/1.json
  def show
  end

  # GET /suppliers/new
  def new
    @supplier = Supplier.new
  end

  # GET /suppliers/1/edit
  def edit
  end

  # POST /suppliers
  # POST /suppliers.json
  def create
    @supplier = Supplier.new(supplier_params)

    respond_to do |format|
      if @supplier.save
        format.html { redirect_to @supplier, notice: 'Lieferant wurde erfolgreich erstellt.' }
        format.json { render :show, status: :created, location: @supplier }
      else
        format.html { render :new }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /suppliers/1
  # PATCH/PUT /suppliers/1.json
  def update
    respond_to do |format|
      if @supplier.update(supplier_params)
        format.html { redirect_to @supplier, notice: 'Lieferant wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @supplier }
      else
        format.html { render :edit }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suppliers/1
  # DELETE /suppliers/1.json
  def destroy
    @supplier.destroy
    respond_to do |format|
      format.html { redirect_to suppliers_url, notice: 'Lieferant wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_supplier
    @supplier = Supplier.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def supplier_params
    params.require(:supplier).permit(:name, :contact_firstname, :contact_lastname, :email, :gams_id,
                                     rawoils_attributes: [:id, :_destroy, :name, :density, :category, :refinery_id, :final_product_id, :supplier_id],
                                     shipments_attributes: [:id, :_destroy, :supplier_id, :rawoil_id, :month_id, :quantity, :price]
    )
  end
end
