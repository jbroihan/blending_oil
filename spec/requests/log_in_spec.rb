require 'rails_helper'

RSpec.configure do |c|
  c.include SessionHelpers
end

feature 'Admin logs in' do
  before do
    create(:role, name: 'Admin')
    create(:role, name: 'Customer')
    create(:role, name: 'Supplier')
    create(:finalProduct)
    5.times { create(:administration) }
    create(:balance)
  end

  scenario 'with valid params' do
    @user = create(:user)
    create(:assignment, user_id: @user.id, role_id: 1)

    sign_in(@user)
    expect(page).to have_content('Produktionsmengen')
  end

  scenario 'with invalid params' do
    @user = build(:user) # user is not saved to database
    create(:assignment, user_id: @user.id, role_id: 1)

    sign_in(@user)
    expect(page).to have_content('Die Kombination aus E-Mail und Passwort existiert nicht.')
  end
end

feature 'Customer logs in' do
  before do
    create(:role, name: 'Admin')
    create(:role, name: 'Customer')
    create(:role, name: 'Supplier')
    create(:finalProduct)
    5.times { create(:administration) }
    create(:balance)
  end

  scenario 'with valid params' do
    @user = create(:user)
    create(:assignment, user_id: @user.id, role_id: 2)

    sign_in(@user)
    expect(page).to have_content('Bestellmengen pro Monat')
  end

  scenario 'with invalid params' do
    @user = build(:user) # user is not saved to database
    create(:assignment, user_id: @user.id, role_id: 2)

    sign_in(@user)
    expect(page).to have_content('Die Kombination aus E-Mail und Passwort existiert nicht.')
  end
end

feature 'Supplier logs in' do
  before do
    create(:role, name: 'Admin')
    create(:role, name: 'Customer')
    create(:role, name: 'Supplier')
    create(:finalProduct)
    5.times { create(:administration) }
    create(:balance)
  end

  scenario 'with valid params' do
    @user = create(:user)
    create(:assignment, user_id: @user.id, role_id: 3)

    sign_in(@user)
    expect(page).to have_content('Umsatz')
  end

  scenario 'with invalid params' do
    @user = build(:user) # user is not saved to database
    create(:assignment, user_id: @user.id, role_id: 3)

    sign_in(@user)
    expect(page).to have_content('Die Kombination aus E-Mail und Passwort existiert nicht.')
  end
end


