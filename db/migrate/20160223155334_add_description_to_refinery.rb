class AddDescriptionToRefinery < ActiveRecord::Migration
  def change
    add_column :refineries, :description, :string
  end
end
