json.array!(@final_products) do |final_product|
  json.extract! final_product, :id, :name, :price
  json.url final_product_url(final_product, format: :json)
end
