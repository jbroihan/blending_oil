json.array!(@storages) do |storage|
  json.extract! storage, :id, :name, :capacity
  json.url storage_url(storage, format: :json)
end
