require 'rails_helper'

describe Customer do
  it 'has a valid factory' do
    expect(build(:customer)).to be_valid
  end

  it 'is invalid without a firstname' do
    expect(build(:customer, namefi: nil)).to_not be_valid
  end

  it 'is invalid without a lastname' do
    expect(build(:customer, namela: nil)).to_not be_valid
  end

  it 'is invalid without an email' do
    expect(build(:customer, email: nil)).to_not be_valid
  end

  it 'is invalid without an adress' do
    expect(build(:customer, adress: nil)).to_not be_valid
  end

  it 'is invalid without a gams_id' do
    expect(build(:customer, gams_id: nil)).to_not be_valid
  end

  it 'returns a customers full name' do
    customer = create(:customer, namefi: 'Jane', namela: 'Do')
    expect(customer.fullname).to eq('Jane Do')
  end


end