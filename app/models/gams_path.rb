class GamsPath < ActiveRecord::Base
  validates :gams_path_url, presence: true
end
