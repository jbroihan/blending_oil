Sets 
 	 m 			/ m1*m6 / 
	 p 			/ p1*p5 / 
	 c 			/ c1*c4 / 
	 s 			/ s1*s3 / 
	 mpa 		/ mpa1*mpa30 / 
	 mpsa 		/ mpsa1*mpsa90 / 
	 mpoa 		/ mpoa1*mpoa30 / 
	 mproda 	/ mproda1*mproda6 / 
	 pv(p) 	 	/ 
 					p1
 					p2 / 
	 pnv(p) 	 / 
 					p3
 					p4
 					p5 /; 
 
 
Parameters 
 	 maxstore 	 / 400.0 / 
	 maxusepv 	 / 200.0  / 
	 maxusepnv 	 / 250.0  / 
	 minusep 	 / 20.0 / 
	 maxnusep 	 / 3.0 / 
	 sp 		 / 150.0 / 
	 sc 		 / 5.0 / 
	 stock(p) 	 / #p 200.0 / 
	 hmin 		 / 0.75 / 
	 hmax 		 / 0.845 / 
	 h(p) 		 / 
 					p1 1.0
 					p2 0.5
 					p3 0.8
 					p4 0.9
 					p5 0.7 /; 


 MP(m,p) = yes;
 MPS(m,p,s) = yes;
 MPO(m,p) = yes;
 MPROD(m) = yes; 


demand('m1','c1') = 80.0;
demand('m2','c1') = 30.0;
demand('m3','c1') = 30.0;
demand('m4','c1') = 60.0;
demand('m5','c1') = 80.0;
demand('m6','c1') = 30.0;
demand('m1','c2') = 80.0;
demand('m2','c2') = 70.0;
demand('m3','c2') = 100.0;
demand('m4','c2') = 100.0;
demand('m5','c2') = 60.0;
demand('m6','c2') = 110.0;
demand('m1','c3') = 130.0;
demand('m2','c3') = 80.0;
demand('m3','c3') = 120.0;
demand('m4','c3') = 100.0;
demand('m5','c3') = 100.0;
demand('m6','c3') = 140.0;
demand('m1','c4') = 140.0;
demand('m2','c4') = 130.0;
demand('m3','c4') = 140.0;
demand('m4','c4') = 150.0;
demand('m5','c4') = 160.0;
demand('m6','c4') = 130.0;


cost('m1','p1','s1') = 100.0;
cost('m1','p2','s1') = 120.0;
cost('m1','p3','s1') = 110.0;
cost('m1','p4','s1') = 150.0;
cost('m1','p5','s1') = 140.0;
cost('m2','p1','s1') = 120.0;
cost('m2','p2','s1') = 140.0;
cost('m2','p3','s1') = 90.0;
cost('m2','p4','s1') = 130.0;
cost('m2','p5','s1') = 120.0;
cost('m3','p1','s1') = 130.0;
cost('m3','p2','s1') = 100.0;
cost('m3','p3','s1') = 120.0;
cost('m3','p4','s1') = 180.0;
cost('m3','p5','s1') = 110.0;
cost('m4','p1','s1') = 90.0;
cost('m4','p2','s1') = 100.0;
cost('m4','p3','s1') = 150.0;
cost('m4','p4','s1') = 140.0;
cost('m4','p5','s1') = 120.0;
cost('m5','p1','s1') = 120.0;
cost('m5','p2','s1') = 140.0;
cost('m5','p3','s1') = 100.0;
cost('m5','p4','s1') = 120.0;
cost('m5','p5','s1') = 160.0;
cost('m6','p1','s1') = 100.0;
cost('m6','p2','s1') = 115.0;
cost('m6','p3','s1') = 150.0;
cost('m6','p4','s1') = 80.0;
cost('m6','p5','s1') = 130.0;
cost('m1','p1','s2') = 110.0;
cost('m1','p2','s2') = 140.0;
cost('m1','p3','s2') = 100.0;
cost('m1','p4','s2') = 140.0;
cost('m1','p5','s2') = 110.0;
cost('m2','p1','s2') = 130.0;
cost('m2','p2','s2') = 170.0;
cost('m2','p3','s2') = 90.0;
cost('m2','p4','s2') = 110.0;
cost('m2','p5','s2') = 130.0;
cost('m3','p1','s2') = 110.0;
cost('m3','p2','s2') = 140.0;
cost('m3','p3','s2') = 100.0;
cost('m3','p4','s2') = 150.0;
cost('m3','p5','s2') = 140.0;
cost('m4','p1','s2') = 100.0;
cost('m4','p2','s2') = 120.0;
cost('m4','p3','s2') = 90.0;
cost('m4','p4','s2') = 170.0;
cost('m4','p5','s2') = 120.0;
cost('m5','p1','s2') = 110.0;
cost('m5','p2','s2') = 140.0;
cost('m5','p3','s2') = 130.0;
cost('m5','p4','s2') = 140.0;
cost('m5','p5','s2') = 130.0;
cost('m6','p1','s2') = 110.0;
cost('m6','p2','s2') = 190.0;
cost('m6','p3','s2') = 100.0;
cost('m6','p4','s2') = 90.0;
cost('m6','p5','s2') = 100.0;
cost('m1','p1','s3') = 90.0;
cost('m1','p2','s3') = 130.0;
cost('m1','p3','s3') = 80.0;
cost('m1','p4','s3') = 110.0;
cost('m1','p5','s3') = 100.0;
cost('m2','p1','s3') = 130.0;
cost('m2','p2','s3') = 110.0;
cost('m2','p3','s3') = 120.0;
cost('m2','p4','s3') = 95.0;
cost('m2','p5','s3') = 150.0;
cost('m3','p1','s3') = 110.0;
cost('m3','p2','s3') = 110.0;
cost('m3','p3','s3') = 75.0;
cost('m3','p4','s3') = 110.0;
cost('m3','p5','s3') = 120.0;
cost('m4','p1','s3') = 100.0;
cost('m4','p2','s3') = 900.0;
cost('m4','p3','s3') = 120.0;
cost('m4','p4','s3') = 130.0;
cost('m4','p5','s3') = 170.0;
cost('m5','p1','s3') = 140.0;
cost('m5','p2','s3') = 130.0;
cost('m5','p3','s3') = 80.0;
cost('m5','p4','s3') = 130.0;
cost('m5','p5','s3') = 100.0;
cost('m6','p1','s3') = 90.0;
cost('m6','p2','s3') = 80.0;
cost('m6','p3','s3') = 160.0;
cost('m6','p4','s3') = 140.0;
cost('m6','p5','s3') = 120.0;


 MPaMP(mpa,m,p) = no;

MPaMP('mpa1','m1','p1') = yes; 
MPaMP('mpa2','m2','p1') = yes; 
MPaMP('mpa3','m3','p1') = yes; 
MPaMP('mpa4','m4','p1') = yes; 
MPaMP('mpa5','m5','p1') = yes; 
MPaMP('mpa6','m6','p1') = yes; 
MPaMP('mpa7','m1','p2') = yes; 
MPaMP('mpa8','m2','p2') = yes; 
MPaMP('mpa9','m3','p2') = yes; 
MPaMP('mpa10','m4','p2') = yes; 
MPaMP('mpa11','m5','p2') = yes; 
MPaMP('mpa12','m6','p2') = yes; 
MPaMP('mpa13','m1','p3') = yes; 
MPaMP('mpa14','m2','p3') = yes; 
MPaMP('mpa15','m3','p3') = yes; 
MPaMP('mpa16','m4','p3') = yes; 
MPaMP('mpa17','m5','p3') = yes; 
MPaMP('mpa18','m6','p3') = yes; 
MPaMP('mpa19','m1','p4') = yes; 
MPaMP('mpa20','m2','p4') = yes; 
MPaMP('mpa21','m3','p4') = yes; 
MPaMP('mpa22','m4','p4') = yes; 
MPaMP('mpa23','m5','p4') = yes; 
MPaMP('mpa24','m6','p4') = yes; 
MPaMP('mpa25','m1','p5') = yes; 
MPaMP('mpa26','m2','p5') = yes; 
MPaMP('mpa27','m3','p5') = yes; 
MPaMP('mpa28','m4','p5') = yes; 
MPaMP('mpa29','m5','p5') = yes; 
MPaMP('mpa30','m6','p5') = yes; 


MPSaMPS(mpsa,m,p,s) = no; 
 
MPSaMPS('mpsa1','m1','p1','s1') = yes; 
MPSaMPS('mpsa2','m1','p2','s1') = yes; 
MPSaMPS('mpsa3','m1','p3','s1') = yes; 
MPSaMPS('mpsa4','m1','p4','s1') = yes; 
MPSaMPS('mpsa5','m1','p5','s1') = yes; 
MPSaMPS('mpsa6','m2','p1','s1') = yes; 
MPSaMPS('mpsa7','m2','p2','s1') = yes; 
MPSaMPS('mpsa8','m2','p3','s1') = yes; 
MPSaMPS('mpsa9','m2','p4','s1') = yes; 
MPSaMPS('mpsa10','m2','p5','s1') = yes; 
MPSaMPS('mpsa11','m3','p1','s1') = yes; 
MPSaMPS('mpsa12','m3','p2','s1') = yes; 
MPSaMPS('mpsa13','m3','p3','s1') = yes; 
MPSaMPS('mpsa14','m3','p4','s1') = yes; 
MPSaMPS('mpsa15','m3','p5','s1') = yes; 
MPSaMPS('mpsa16','m4','p1','s1') = yes; 
MPSaMPS('mpsa17','m4','p2','s1') = yes; 
MPSaMPS('mpsa18','m4','p3','s1') = yes; 
MPSaMPS('mpsa19','m4','p4','s1') = yes; 
MPSaMPS('mpsa20','m4','p5','s1') = yes; 
MPSaMPS('mpsa21','m5','p1','s1') = yes; 
MPSaMPS('mpsa22','m5','p2','s1') = yes; 
MPSaMPS('mpsa23','m5','p3','s1') = yes; 
MPSaMPS('mpsa24','m5','p4','s1') = yes; 
MPSaMPS('mpsa25','m5','p5','s1') = yes; 
MPSaMPS('mpsa26','m6','p1','s1') = yes; 
MPSaMPS('mpsa27','m6','p2','s1') = yes; 
MPSaMPS('mpsa28','m6','p3','s1') = yes; 
MPSaMPS('mpsa29','m6','p4','s1') = yes; 
MPSaMPS('mpsa30','m6','p5','s1') = yes; 
MPSaMPS('mpsa31','m1','p1','s2') = yes; 
MPSaMPS('mpsa32','m1','p2','s2') = yes; 
MPSaMPS('mpsa33','m1','p3','s2') = yes; 
MPSaMPS('mpsa34','m1','p4','s2') = yes; 
MPSaMPS('mpsa35','m1','p5','s2') = yes; 
MPSaMPS('mpsa36','m2','p1','s2') = yes; 
MPSaMPS('mpsa37','m2','p2','s2') = yes; 
MPSaMPS('mpsa38','m2','p3','s2') = yes; 
MPSaMPS('mpsa39','m2','p4','s2') = yes; 
MPSaMPS('mpsa40','m2','p5','s2') = yes; 
MPSaMPS('mpsa41','m3','p1','s2') = yes; 
MPSaMPS('mpsa42','m3','p2','s2') = yes; 
MPSaMPS('mpsa43','m3','p3','s2') = yes; 
MPSaMPS('mpsa44','m3','p4','s2') = yes; 
MPSaMPS('mpsa45','m3','p5','s2') = yes; 
MPSaMPS('mpsa46','m4','p1','s2') = yes; 
MPSaMPS('mpsa47','m4','p2','s2') = yes; 
MPSaMPS('mpsa48','m4','p3','s2') = yes; 
MPSaMPS('mpsa49','m4','p4','s2') = yes; 
MPSaMPS('mpsa50','m4','p5','s2') = yes; 
MPSaMPS('mpsa51','m5','p1','s2') = yes; 
MPSaMPS('mpsa52','m5','p2','s2') = yes; 
MPSaMPS('mpsa53','m5','p3','s2') = yes; 
MPSaMPS('mpsa54','m5','p4','s2') = yes; 
MPSaMPS('mpsa55','m5','p5','s2') = yes; 
MPSaMPS('mpsa56','m6','p1','s2') = yes; 
MPSaMPS('mpsa57','m6','p2','s2') = yes; 
MPSaMPS('mpsa58','m6','p3','s2') = yes; 
MPSaMPS('mpsa59','m6','p4','s2') = yes; 
MPSaMPS('mpsa60','m6','p5','s2') = yes; 
MPSaMPS('mpsa61','m1','p1','s3') = yes; 
MPSaMPS('mpsa62','m1','p2','s3') = yes; 
MPSaMPS('mpsa63','m1','p3','s3') = yes; 
MPSaMPS('mpsa64','m1','p4','s3') = yes; 
MPSaMPS('mpsa65','m1','p5','s3') = yes; 
MPSaMPS('mpsa66','m2','p1','s3') = yes; 
MPSaMPS('mpsa67','m2','p2','s3') = yes; 
MPSaMPS('mpsa68','m2','p3','s3') = yes; 
MPSaMPS('mpsa69','m2','p4','s3') = yes; 
MPSaMPS('mpsa70','m2','p5','s3') = yes; 
MPSaMPS('mpsa71','m3','p1','s3') = yes; 
MPSaMPS('mpsa72','m3','p2','s3') = yes; 
MPSaMPS('mpsa73','m3','p3','s3') = yes; 
MPSaMPS('mpsa74','m3','p4','s3') = yes; 
MPSaMPS('mpsa75','m3','p5','s3') = yes; 
MPSaMPS('mpsa76','m4','p1','s3') = yes; 
MPSaMPS('mpsa77','m4','p2','s3') = yes; 
MPSaMPS('mpsa78','m4','p3','s3') = yes; 
MPSaMPS('mpsa79','m4','p4','s3') = yes; 
MPSaMPS('mpsa80','m4','p5','s3') = yes; 
MPSaMPS('mpsa81','m5','p1','s3') = yes; 
MPSaMPS('mpsa82','m5','p2','s3') = yes; 
MPSaMPS('mpsa83','m5','p3','s3') = yes; 
MPSaMPS('mpsa84','m5','p4','s3') = yes; 
MPSaMPS('mpsa85','m5','p5','s3') = yes; 
MPSaMPS('mpsa86','m6','p1','s3') = yes; 
MPSaMPS('mpsa87','m6','p2','s3') = yes; 
MPSaMPS('mpsa88','m6','p3','s3') = yes; 
MPSaMPS('mpsa89','m6','p4','s3') = yes; 
MPSaMPS('mpsa90','m6','p5','s3') = yes; 


MPOaMPO(mpoa,m,p) = no; 
 
MPOaMPO('mpoa1','m1','p1') = yes; 
MPOaMPO('mpoa2','m2','p1') = yes; 
MPOaMPO('mpoa3','m3','p1') = yes; 
MPOaMPO('mpoa4','m4','p1') = yes; 
MPOaMPO('mpoa5','m5','p1') = yes; 
MPOaMPO('mpoa6','m6','p1') = yes; 
MPOaMPO('mpoa7','m1','p2') = yes; 
MPOaMPO('mpoa8','m2','p2') = yes; 
MPOaMPO('mpoa9','m3','p2') = yes; 
MPOaMPO('mpoa10','m4','p2') = yes; 
MPOaMPO('mpoa11','m5','p2') = yes; 
MPOaMPO('mpoa12','m6','p2') = yes; 
MPOaMPO('mpoa13','m1','p3') = yes; 
MPOaMPO('mpoa14','m2','p3') = yes; 
MPOaMPO('mpoa15','m3','p3') = yes; 
MPOaMPO('mpoa16','m4','p3') = yes; 
MPOaMPO('mpoa17','m5','p3') = yes; 
MPOaMPO('mpoa18','m6','p3') = yes; 
MPOaMPO('mpoa19','m1','p4') = yes; 
MPOaMPO('mpoa20','m2','p4') = yes; 
MPOaMPO('mpoa21','m3','p4') = yes; 
MPOaMPO('mpoa22','m4','p4') = yes; 
MPOaMPO('mpoa23','m5','p4') = yes; 
MPOaMPO('mpoa24','m6','p4') = yes; 
MPOaMPO('mpoa25','m1','p5') = yes; 
MPOaMPO('mpoa26','m2','p5') = yes; 
MPOaMPO('mpoa27','m3','p5') = yes; 
MPOaMPO('mpoa28','m4','p5') = yes; 
MPOaMPO('mpoa29','m5','p5') = yes; 
MPOaMPO('mpoa30','m6','p5') = yes; 


MPRODaMPROD(mproda,m) = no; 
 
MPRODaMPROD('mproda1','m1') = yes; 
MPRODaMPROD('mproda2','m2') = yes; 
MPRODaMPROD('mproda3','m3') = yes; 
MPRODaMPROD('mproda4','m4') = yes; 
MPRODaMPROD('mproda5','m5') = yes; 
MPRODaMPROD('mproda6','m6') = yes; 


