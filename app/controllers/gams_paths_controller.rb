class GamsPathsController < ApplicationController
  load_and_authorize_resource
  before_action :set_gams_path, only: [:show, :edit, :update]

  # GET /gams_paths
  # GET /gams_paths.json
  def index
    @gams_paths = GamsPath.all

    respond_to do |format|
      format.html
      format.xls { send_data(@gams_paths.to_xls) }
      format.xls {
        filename = "Gams-paths-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@gams_paths.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /gams_paths/1
  # GET /gams_paths/1.json
  def show
  end

  # GET /gams_paths/1/edit
  def edit
  end

  # PATCH/PUT /gams_paths/1
  # PATCH/PUT /gams_paths/1.json
  def update
    respond_to do |format|
      if @gams_path.update(gams_path_params)
        format.html { redirect_to @gams_path, notice: 'GAMS-Pfad wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @gams_path }
      else
        format.html { render :edit }
        format.json { render json: @gams_path.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_gams_path
    @gams_path = GamsPath.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def gams_path_params
    params.require(:gams_path).permit(:gams_path_url)
  end
end
