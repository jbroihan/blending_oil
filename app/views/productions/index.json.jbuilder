json.array!(@productions) do |production|
  json.extract! production, :id, :month_id, :production
  json.url production_url(production, format: :json)
end
