class ProductPeriodAssociationsController < ApplicationController
  before_action :set_product_period_association, only: [:show, :edit, :update, :destroy]

  # GET /product_period_associations
  # GET /product_period_associations.json
  def index
    @product_period_associations = ProductPeriodAssociation.all
  end

  # GET /product_period_associations/1
  # GET /product_period_associations/1.json
  def show
  end

  # GET /product_period_associations/new
  def new
    @product_period_association = ProductPeriodAssociation.new
  end

  # GET /product_period_associations/1/edit
  def edit
  end

  # POST /product_period_associations
  # POST /product_period_associations.json
  def create
    @product_period_association = ProductPeriodAssociation.new(product_period_association_params)

    respond_to do |format|
      if @product_period_association.save
        format.html { redirect_to @product_period_association, notice: 'Product period association was successfully created.' }
        format.json { render :show, status: :created, location: @product_period_association }
      else
        format.html { render :new }
        format.json { render json: @product_period_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_period_associations/1
  # PATCH/PUT /product_period_associations/1.json
  def update
    respond_to do |format|
      if @product_period_association.update(product_period_association_params)
        format.html { redirect_to @product_period_association, notice: 'Product period association was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_period_association }
      else
        format.html { render :edit }
        format.json { render json: @product_period_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_period_associations/1
  # DELETE /product_period_associations/1.json
#  def destroy
#    @product_period_association.destroy
#    respond_to do |format|
#      format.html { redirect_to product_period_associations_url, notice: 'Product period association was successfully destroyed.' }
#      format.json { head :no_content }
#    end
#  end



  def optimize

    if File.exist?("MLCLSP_Input.inc")
      File.delete("MLCLSP_Input.inc")
    end
    f=File.new("MLCLSP_Input.inc", "w")
    printf(f, "set k / \n")
    @products = Product.all
    @products.each { |prod| printf(f, prod.name + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "set t / \n")
    @periods = Period.all
    @periods.each { |per| printf(f, per.name + "\n") }
    printf(f, "/" + "\n\n")


    printf(f, "set j / \n")
    @machines = Machine.all
    @machines.each { |mac| printf(f, mac.name + "\n") }
    printf(f, "/;\n\n")


    printf(f, "KT(k,t)=yes;\n")
    printf(f, "JT(j,t)=yes;\n\n")
    printf(f, "KJ(k,j)=no;\n")
    printf(f, "KK(k,k)=no;\n\n")

    printf(f, "set kta / \n")
    @product_period_association = ProductPeriodAssociation.all
    @product_period_association.each { |prod_per| printf(f, "kta" + prod_per.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "KTaKT(kta,k,t)=no;" + "\n\n")
    @product_period_association.each { |prod_per| printf(f, "KTaKT('kta" + prod_per.id.to_s + "','" + prod_per.product.name + "','" + prod_per.period.name + "')=yes;\n") }
    printf(f, "\n\n")


    printf(f, "set jta / \n")
    @machine_period_associations = MachinePeriodAssociation.all
    @machine_period_associations.each { |mac_per| printf(f, "jta" + mac_per.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "JTaJT(jta,j,t)=no;" + "\n\n")
    @machine_period_associations.each { |mac_per| printf(f, "JTaJT('jta" + mac_per.id.to_s + "','" + mac_per.machine.name + "','" + mac_per.period.name + "')=yes;\n") }
    printf(f, "\n\n")


    @products = Product.all
    @products.each { |pro| printf(f, "KJ('" + pro.name+"','" + pro.machine.name+"')=yes;\n") }

    printf(f, "\n")

    printf(f, "a(k,i)=0;\n")

    @product_product_associations = ProductProductAssociation.all
    @product_product_associations.each { |pro_pro|
      printf(f, "KK('" + pro_pro.predecessor.name+"','" + pro_pro.successor.name+"')=yes;\n")
      printf(f, "a('" + pro_pro.predecessor.name+"','" + pro_pro.successor.name+"')= "+ pro_pro.coefficient.to_s + ";\n")
    }

    printf(f, "\n")

    @products.each { |prod|
      printf(f, "ts('" + prod.name + "')= "+ prod.setup_time.to_s + ";\n")
      printf(f, "tp('" + prod.name + "')= "+ prod.processing_time.to_s + ";\n")
      printf(f, "s('" + prod.name + "')= "+ prod.setup_cost.to_s + ";\n")
      printf(f, "h('" + prod.name + "')= "+ prod.holding_cost.to_s + ";\n")
      printf(f, "y_0('" + prod.name + "')= "+ prod.initial_inventory.to_s + ";\n")
      printf(f, "z('" + prod.name + "')= "+ prod.lead_time_periods.to_s + ";\n")
      printf(f, "\n")

    }

    printf(f, "\n")

    @machines.each { |mac|
      printf(f, "oc('" + mac.name + "')= "+ mac.overtime_cost.to_s + ";\n")
    }

    printf(f, "\n")


    @machine_period_associations = MachinePeriodAssociation.all
    @machine_period_associations.each { |mac_per|
      printf(f, "C('" +mac_per.machine.name+"','" + mac_per.period.name+"')= "+ mac_per.capacity.to_s + ";\n")
    }

    printf(f, "\n")


    @product_period_associations = ProductPeriodAssociation.all
    @product_period_associations.each { |pro_per|
      printf(f, "d('" +pro_per.product.name+"','" + pro_per.period.name+"')= "+ pro_per.demand.to_s + ";\n")
    }


    f.close


    if File.exist?("MLClSP_solution.txt")
      File.delete("MLCLSP_solution.txt")
    end

    system "C:\\GAMS\\win64\\24.5\\gams MLCLSP_omapps"

    flash.now[:started] = "Die Rechnung wurde gestartet!"

    render 'static_pages/mlclsp_start'


  end

  def delete_old_plan

    if File.exist?("MLCLSP_solution_kt.txt")
      File.delete("MLCLSP_solution_kt.txt")
    end

    if File.exist?("MLCLSP_solution_jt.txt")
      File.delete("MLCLSP_solution_jt.txt")
    end

    if File.exist?("MLCLSP_ofv.txt")
      File.delete("MLCLSP_ofv.txt")
    end

    @product_period_associations = ProductPeriodAssociation.all
    @product_period_associations.each { |pro_per|
      pro_per.production_quantity=nil
      pro_per.inventory_level=nil
      pro_per.setup_variable=nil
      pro_per.save
    }

    @machine_period_associations = MachinePeriodAssociation.all
    @machine_period_associations.each { |mac_per|
      mac_per.overtime=nil
      mac_per.save
    }

    @objective_function_value=nil

    render :template => "product_period_associations/index"

  end


  def read_optimization_results


    if (File.exist?("MLCLSP_solution_kt.txt") and File.exists?("MLCLSP_solution_jt.txt"))

      fi=File.open("MLCLSP_solution_kt.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "kta "
        sa3=sa[3]
        sa4=sa[4]
        sa5=sa[5].delete " \n"
        product_period_association=ProductPeriodAssociation.find_by_id(sa0)
        product_period_association.production_quantity = sa3
        product_period_association.inventory_level = sa4
        product_period_association.setup_variable = sa5
        product_period_association.save
      }
      fi.close

      fi=File.open("MLCLSP_solution_jt.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "jta "
        sa3=sa[3].delete " \n"
        machine_period_association=MachinePeriodAssociation.find_by_id(sa0)
        machine_period_association.overtime = sa3
        machine_period_association.save
      }
      fi.close

    else
      flash.now[:not_available] = "Die Lösung wurde noch nicht berechnet!"
    end
    @product_period_associations = ProductPeriodAssociation.all
    render :template => "product_period_associations/index"

  end


  def read_and_show_ofv

    if File.exist?("MLCLSP_ofv.txt")
      fi=File.open("MLCLSP_ofv.txt", "r")
      line=fi.readline
      fi.close
      sa=line.split(" ")
      @objective_function_value=sa[1]
    else
      @objective_function_value=nil
      flash.now[:not_available] = "Zielfunktionswert wurde noch nicht berechnet!"

    end

    @product_period_associations = ProductPeriodAssociation.all
    render :template => "product_period_associations/index"
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_period_association
      @product_period_association = ProductPeriodAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_period_association_params
      params.require(:product_period_association).permit(:product_id, :period_id, :demand, :production_quantity, :inventory_level, :setup_variable)
    end
end
