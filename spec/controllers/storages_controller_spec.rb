require 'rails_helper'

describe StoragesController, :type => :controller do

  before do
    3.times { create(:administration) }
  end

  describe 'GET #index' do
    it 'populates an array of storages' do
      storage = create(:storage)
      get :index
      expect(assigns(:storages)).to eq([storage])
    end
    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe 'GET #show' do
    let(:storage) { create(:storage) }
    before { get :show, id: storage.id }
    it 'assigns the requested storage to @storage' do
      expect(assigns(:storage)).to eq storage
    end
    it 'renders the #show view' do
      expect(response).to render_template(:show)
    end
  end

  describe 'GET #new' do
    let(:storage) { create(:storage) }
    it 'assigns a new storage to @storage' do
      get :new, id: storage.id
      expect(assigns(:storage)).to be_a_new(Storage)
    end
    it 'renders the #new view' do
      get :new, id: storage.id
      expect(response).to render_template(:new)
    end
  end

  describe 'GET edit' do
    let(:storage) { create(:storage) }
    it 'assigns storage to @storage' do
      get :edit, id: storage.id
      expect(assigns(:storage)).to eq storage
    end
  end

  describe 'POST create' do
    context 'with valid attributes' do
      it 'creates a new storage' do
        expect{ post :create, :storage => attributes_for(:storage) }.to change(Storage, :count).by(1)
      end

      it 'redirects to the new storage' do
        post :create, storage: attributes_for(:storage)
        expect(response).to redirect_to Storage.last
      end
    end

    context 'with invalid attributes' do
      before { create(:storage) } # to call next_id on storage
      it 'does not save the new storage' do
        expect{ post :create, storage: attributes_for(:invalid_storage) }.to_not change(Storage,:count)
      end

      it 're-renders the new method' do
        post :create, storage: attributes_for(:invalid_storage)
        expect(response).to render_template(:new)
      end
    end
  end
end

