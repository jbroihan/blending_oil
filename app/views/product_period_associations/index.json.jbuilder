json.array!(@product_period_associations) do |product_period_association|
  json.extract! product_period_association, :id, :product_id, :period_id, :demand, :production_quantity, :inventory_level, :setup_variable
  json.url product_period_association_url(product_period_association, format: :json)
end
