class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user ||= User.new # guest user (not logged in)
    @user.roles.each { |role| send(role.name.downcase) }

    if @user.roles.size == 0
      can :index, :all
      can [:new, :create], User
    end
  end

  def customer
    can :index, FinalProduct
    can [:index, :create, :edit, :update, :new, :show, :be, :destroy], Customer
    can [:index, :show, :create, :edit, :update, :destroy, :view], Order
    can [:show, :edit, :update, :destroy], User
  end

  def supplier
    can [:index, :create, :edit, :update, :new, :show, :be, :destroy], Supplier
    can [:show, :edit, :update, :destroy], User
    can [:index, :show, :create, :edit, :update, :destroy, :view], Shipment
  end

  def admin
    can :index, :all
    can :show, :all
    can :edit, :all
    can :update, :all
    can :create, :all
    can :destroy, :all
    can :_destroy, :all
    can [:index, :show, :new, :edit, :update, :create, :optimize, :delete_old_plan], Warehousing
  end

  def guest
    can [:index, :new, :create, :edit, :update], User
  end
end
