require 'rails_helper'

describe Order do
  it 'has a valid factory' do
    expect(create(:order)).to be_valid
  end

  it 'is invalid without a customer_id' do
    expect(build(:order, customer_id: nil)).to_not be_valid
  end

  it 'is invalid without a month_id' do
    expect(build(:order, month_id: nil)).to_not be_valid
  end

  it 'is invalid without a final_product_id' do
    expect(build(:order, final_product_id: nil)).to_not be_valid
  end

  it 'is invalid without a quantity' do
    expect(build(:order, quantity: nil)).to_not be_valid
  end

  it 'does not allow multiple entries for a combination of customer and month' do
    customer = create(:customer)
    create(:order, customer: customer, month_id: 1)
    expect(build(:order, customer: customer, month_id: 1)).not_to be_valid
  end

end