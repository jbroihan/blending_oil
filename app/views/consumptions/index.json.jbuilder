json.array!(@consumptions) do |consumption|
  json.extract! consumption, :id, :month_id, :rawoil_id, :induse, :use
  json.url consumption_url(consumption, format: :json)
end
