require 'test_helper'

class RawoilsControllerTest < ActionController::TestCase
  setup do
    @rawoil = rawoils(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rawoils)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rawoil" do
    assert_difference('Rawoil.count') do
      post :create, rawoil: { density: @rawoil.density, name: @rawoil.name }
    end

    assert_redirected_to rawoil_path(assigns(:rawoil))
  end

  test "should show rawoil" do
    get :show, id: @rawoil
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rawoil
    assert_response :success
  end

  test "should update rawoil" do
    patch :update, id: @rawoil, rawoil: { density: @rawoil.density, name: @rawoil.name }
    assert_redirected_to rawoil_path(assigns(:rawoil))
  end

  test "should destroy rawoil" do
    assert_difference('Rawoil.count', -1) do
      delete :destroy, id: @rawoil
    end

    assert_redirected_to rawoils_path
  end
end
