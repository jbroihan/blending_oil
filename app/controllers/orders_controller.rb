class OrdersController < ApplicationController
  load_and_authorize_resource
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    # If current_user is admin, index all orders
    if can? :optimize, Warehousing
      @orders = Order.all
      @customers = Customer.all
    else
      # If current_user is not admin, index only those matching current_users name
      @orders = Order.joins(:customer).where(customers: {namefi: current_user.firstname, namela: current_user.name})
      @customers = Customer.where(customers: {namefi: current_user.firstname, namela: current_user.name})
    end

    respond_to do |format|
      format.html
      format.xls { send_data(@orders.to_xls) }
      format.xls {
        filename = "Orders-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@orders.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @orders = Order.new
    if cannot? :optimize, Warehousing
      @customer = Customer.where(customers: {namefi: current_user.firstname, namela: current_user.name})
    end
  end

  # GET /orders/1/edit
  def edit
    @customer = Customer.where(customers: {namefi: current_user.firstname, namela: current_user.name})
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Kundenbestellung wurde erfolgreich erstellt.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Kundenbestellung wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Kundenbestellung wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    params.require(:order).permit(:customer_id, :final_product_id, :month_id, :quantity)
  end

end
