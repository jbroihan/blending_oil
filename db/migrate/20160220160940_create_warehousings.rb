class CreateWarehousings < ActiveRecord::Migration
  def change
    create_table :warehousings do |t|
      t.integer :rawoil_id
      t.integer :storage_id
      t.string :month_id
      t.float :stock

      t.timestamps null: false
    end
  end
end
