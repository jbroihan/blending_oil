module ShipmentsHelper
  def rawoil_purchase_data(user)
    regarded_supplier = Supplier.where(suppliers: {contact_firstname: user.firstname, contact_lastname: user.name})

    Rawoil.all.map { |rawoil|
      {
          name: rawoil.name,
          data: rawoil.shipments.where(supplier_id: regarded_supplier.pluck(:id)).group(:month_id).sum(:quantity)
      }}
  end
end
