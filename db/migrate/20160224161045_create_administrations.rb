class CreateAdministrations < ActiveRecord::Migration
  def change
    create_table :administrations do |t|
      t.string :category

      t.timestamps null: false
    end
  end
end
