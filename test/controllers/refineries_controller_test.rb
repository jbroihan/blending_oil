require 'test_helper'

class RefineriesControllerTest < ActionController::TestCase
  setup do
    @refinery = refineries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:refineries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create refinery" do
    assert_difference('Refinery.count') do
      post :create, refinery: { capacity: @refinery.capacity, name: @refinery.name, type: @refinery.type }
    end

    assert_redirected_to refinery_path(assigns(:refinery))
  end

  test "should show refinery" do
    get :show, id: @refinery
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @refinery
    assert_response :success
  end

  test "should update refinery" do
    patch :update, id: @refinery, refinery: { capacity: @refinery.capacity, name: @refinery.name, type: @refinery.type }
    assert_redirected_to refinery_path(assigns(:refinery))
  end

  test "should destroy refinery" do
    assert_difference('Refinery.count', -1) do
      delete :destroy, id: @refinery
    end

    assert_redirected_to refineries_path
  end
end
