class Month < ActiveRecord::Base
  validates :name, :description, presence: true

  # n:m Month, Rawoil and Supplier
  has_many :shipments, :dependent => :destroy 
  has_many :suppliers, :through => :shipments
  has_many :rawoils, :through => :shipments

  # n:m  Month, Customer and Final Product
  has_many :orders, :dependent => :destroy
  has_many :customers, :through => :orders
  has_many :final_products, :through => :orders

  # n:m Month, Storage and Rawoil
  has_many :warehousings, :dependent => :destroy
  has_many :storages, :through => :warehousings
  has_many :rawoils, :through => :warehousings

  # Consumptions
  has_many :consumptions, :dependent => :destroy
  has_many :rawoils, :through => :consumptions

  # Productions
  has_one :production, :dependent => :destroy

end
