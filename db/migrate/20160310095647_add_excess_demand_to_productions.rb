class AddExcessDemandToProductions < ActiveRecord::Migration
  def change
    add_column :productions, :excess_demand, :float
  end
end
