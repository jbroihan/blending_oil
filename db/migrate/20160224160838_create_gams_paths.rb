class CreateGamsPaths < ActiveRecord::Migration
  def change
    create_table :gams_paths do |t|
      t.string :gams_path_url

      t.timestamps null: false
    end
  end
end
