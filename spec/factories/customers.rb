require 'faker'

FactoryGirl.define do
  factory :customer do
    namefi  { Faker::Name.first_name }
    namela  { Faker::Name.last_name }
    email   { Faker::Internet.email }
    adress  { Faker::Address.street_address + ', ' + Faker::Address.zip_code + ' ' + Faker::Address.city }
    gams_id { 'c' + Faker::Number.number(1) }
  end

  factory :invalid_customer, parent: :customer do
    namefi nil
  end
end