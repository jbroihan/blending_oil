class MachinePeriodAssociation < ActiveRecord::Base
  belongs_to :machine
  belongs_to :period
end
