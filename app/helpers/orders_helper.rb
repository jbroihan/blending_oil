module OrdersHelper

  def ordered_quantities_chart_data(user)
    regarded_customer = Customer.where(customers: {namefi: user.firstname, namela: user.name})

    regarded_customer.map { |customer|
      {
          name: customer.namefi + ' ' + customer.namela,
          data: customer.orders.group(:month_id).sum(:quantity)
      }}
  end
end
