require 'faker'

FactoryGirl.define do
  factory :storage do
    name        { 't' + Faker::Number.number(1) }
    capacity    { Faker::Number.between(300, 2000) }
    description { Faker::Lorem.word }
  end

  factory :invalid_storage, parent: :storage do
  capacity      { Faker::Number.negative }
  end
end