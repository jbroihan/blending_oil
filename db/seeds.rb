#encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "User",
             firstname: "Admin",
             email: "admin@blending-oil.org",
             password:              "blending",
             password_confirmation: "blending",
             admin: true)

User.create!(name:  "Helber",
             firstname: "Stefan",
             email: "stefan.helber@prod.uni-hannover.de",
             password:              "geheim",
             password_confirmation: "geheim",
             admin: true)

# Assignments Admin
Assignment.create!(user_id: 1, role_id: 1)
Assignment.create!(user_id: 2, role_id: 1)

# Customer-Accounts
User.create!(name: "Sorglos", firstname: "Susi", email: 'susi.sorglos@gmx.de', password: "geheim", password_confirmation: "geheim")
User.create!(name: "Lustig", firstname: "Alfred", email: 'alfred.lustig@web.de', password: "geheim", password_confirmation: "geheim")
User.create!(name: "Müller", firstname: "Helmut", email: 'helmut.mueller@web.de', password: "geheim", password_confirmation: "geheim")
User.create!(name: "Brockhaus", firstname: "Elisabeth", email: 'elisabeth.brockhaus@gmx.de', password: "geheim", password_confirmation: "geheim")


# Supplier-Accounts
User.create!(name: "Winterscheit", firstname: "Klara", email: 'winterscheit@total.com', password: "geheim", password_confirmation: "geheim")
User.create!(name: "Köhler", firstname: "Herbert", email: 'koehler@bp.com', password: "geheim", password_confirmation: "geheim")
User.create!(name: "Martens", firstname: "Christian", email: 'martens@shell.com', password: "geheim", password_confirmation: "geheim")

S1 = Site.create!(name: "Frankfurt(M)",
                  codename: "FRA")
S2 = Site.create!(name: "Berlin-Tegel",
                  codename: "TLX")
S3 = Site.create!(name: "München",
                  codename: "MUC")
S4 = Site.create!(name: "Bremen",
                  codename: "BRE")
S5 = Site.create!(name: "Cottbus",
                  codename: "CBU")
S6 = Site.create!(name: "Düsseldorf",
                  codename: "DUS")
S7 = Site.create!(name: "Hamburg",
                  codename: "HAM")
S8 = Site.create!(name: "Stuttgart",
                  codename: "STR")


(10..99).each do |n|
  name = "Beispielort-#{n}"
  codename = "C#{n}"
  Site.create!(name: name,
               codename: codename)
end


SuSi1 = Supplysite.create!(site_id: S1.id, supply_quantity: 20)

SuSi2 = Supplysite.create!(site_id: S2.id, supply_quantity: 25)

SuSi3 = Supplysite.create!(site_id: S3.id, supply_quantity: 21)

DeSi1 = Demandsite.create!(site_id: S4.id, demand_quantity: 15)

DeSi2 = Demandsite.create!(site_id: S5.id, demand_quantity: 17)

DeSi3 = Demandsite.create!(site_id: S6.id, demand_quantity: 22)

DeSi4 = Demandsite.create!(site_id: S7.id, demand_quantity: 12)

TraLi1 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi1.id, unit_cost: 6, transport_quantity: 0.0)
TraLi2 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi2.id, unit_cost: 2, transport_quantity: 0.0)
TraLi3 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi3.id, unit_cost: 6, transport_quantity: 0.0)
TraLi4 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi4.id, unit_cost: 7, transport_quantity: 0.0)
TraLi5 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi1.id, unit_cost: 4, transport_quantity: 0.0)
TraLi6 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi2.id, unit_cost: 9, transport_quantity: 0.0)
TraLi7 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi3.id, unit_cost: 5, transport_quantity: 0.0)
TraLi8 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi4.id, unit_cost: 3, transport_quantity: 0.0)
TraLi9 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi1.id, unit_cost: 8, transport_quantity: 0.0)
TraLi10 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi2.id, unit_cost: 8, transport_quantity: 0.0)
TraLi11 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi3.id, unit_cost: 1, transport_quantity: 0.0)
TraLi12 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi4.id, unit_cost: 6, transport_quantity: 0.0)


Prod1 = Product.create!(name: "P1", setup_time: 20, processing_time: 1,
                        setup_cost: 200, holding_cost: 40,
                        initial_inventory: 50, lead_time_periods: 1, machine_id: 1)

Prod2 = Product.create!(name: "P2", setup_time: 30, processing_time: 1,
                        setup_cost: 500, holding_cost: 35,
                        initial_inventory: 120, lead_time_periods: 1, machine_id: 2)

Prod3 = Product.create!(name: "P3", setup_time: 10, processing_time: 1,
                        setup_cost: 300, holding_cost: 15,
                        initial_inventory: 100, lead_time_periods: 1, machine_id: 2)

Prod4 = Product.create!(name: "P4", setup_time: 20, processing_time: 1,
                        setup_cost: 1000, holding_cost: 4,
                        initial_inventory: 110, lead_time_periods: 1, machine_id: 2)

Prod5 = Product.create!(name: "P5", setup_time: 10, processing_time: 1,
                        setup_cost: 300, holding_cost: 3,
                        initial_inventory: 80, lead_time_periods: 1, machine_id: 1)


(1..4).each do |n|
  name = "t#{n}"
  Period.create!(name: name)
end

(1..2).each do |n|
  name = "m#{n}"
  Machine.create!(name: name, overtime_cost: 200)
end

(1..2).each do |m|
  (1..4).each do |n|
    MachinePeriodAssociation.create!(machine_id: m, period_id: n, capacity: 150)
  end
end

P1t1 = ProductPeriodAssociation.create!(product_id: 1, period_id: 1, demand: 10)
P1t2 = ProductPeriodAssociation.create!(product_id: 1, period_id: 2, demand: 20)
P1t3 = ProductPeriodAssociation.create!(product_id: 1, period_id: 3, demand: 30)
P1t4 = ProductPeriodAssociation.create!(product_id: 1, period_id: 4, demand: 80)

P2t1 = ProductPeriodAssociation.create!(product_id: 2, period_id: 1, demand: 20)
P2t2 = ProductPeriodAssociation.create!(product_id: 2, period_id: 2, demand: 80)
P2t3 = ProductPeriodAssociation.create!(product_id: 2, period_id: 3, demand: 50)
P2t4 = ProductPeriodAssociation.create!(product_id: 2, period_id: 4, demand: 90)

P3t1 = ProductPeriodAssociation.create!(product_id: 3, period_id: 1, demand: 0)
P3t2 = ProductPeriodAssociation.create!(product_id: 3, period_id: 2, demand: 0)
P3t3 = ProductPeriodAssociation.create!(product_id: 3, period_id: 3, demand: 30)
P3t4 = ProductPeriodAssociation.create!(product_id: 3, period_id: 4, demand: 0)

P4t1 = ProductPeriodAssociation.create!(product_id: 4, period_id: 1, demand: 0)
P4t2 = ProductPeriodAssociation.create!(product_id: 4, period_id: 2, demand: 0)
P4t3 = ProductPeriodAssociation.create!(product_id: 4, period_id: 3, demand: 0)
P4t4 = ProductPeriodAssociation.create!(product_id: 4, period_id: 4, demand: 80)

P5t1 = ProductPeriodAssociation.create!(product_id: 5, period_id: 1, demand: 0)
P5t2 = ProductPeriodAssociation.create!(product_id: 5, period_id: 2, demand: 0)
P5t3 = ProductPeriodAssociation.create!(product_id: 5, period_id: 3, demand: 0)
P5t4 = ProductPeriodAssociation.create!(product_id: 5, period_id: 4, demand: 0)

P3P1 = ProductProductAssociation.create!(predecessor_id: 3, successor_id:1, coefficient: 1 )
P3P2 = ProductProductAssociation.create!(predecessor_id: 3, successor_id:2, coefficient: 1 )
P4P1 = ProductProductAssociation.create!(predecessor_id: 4, successor_id:1, coefficient: 1 )
P5P3 = ProductProductAssociation.create!(predecessor_id: 5, successor_id:3, coefficient: 1 )
P5P4 = ProductProductAssociation.create!(predecessor_id: 5, successor_id:4, coefficient: 1 )

# Pflanzliche Rohöle permanent erstellen
#P1:
Rawoil.create!(name: "p1", density: 1, category: "PV", refinery_id: 1)
#P2:
Rawoil.create!(name: "p2", density: 0.5, category: "PV", refinery_id: 1)

# Nicht-pflanzliche Rohöle permanent erstellen
#P3:
Rawoil.create!(name: "p3", density: 0.8, category: "PNV", refinery_id: 2)
#P4:
Rawoil.create!(name: "p4", density: 0.9, category: "PNV", refinery_id: 2)
#P5:
Rawoil.create!(name: "p5", density: 0.7, category: "PNV", refinery_id: 2)

# Storages
Storage.create!(name: "t1", capacity: 400, description: "Lagertank 1", rawoil_id: 1)
Storage.create!(name: "t2", capacity: 400, description: "Lagertank 2", rawoil_id: 2)
Storage.create!(name: "t3", capacity: 400, description: "Lagertank 3", rawoil_id: 3)
Storage.create!(name: "t4", capacity: 400, description: "Lagertank 4", rawoil_id: 4)
Storage.create!(name: "t5", capacity: 400, description: "Lagertank 5", rawoil_id: 5)

# Refineries
#R1:
Refinery.create!(name: "r1", capacity: 200, category: "PV", description: "Raffinerie 1")
#R2:
Refinery.create!(name: "r2", capacity: 250, category: "PNV", description: "Raffinerie 2")

# Customers
Customer.create!(namela: 'Sorglos', namefi: 'Susi', email: 'susi.sorglos@gmx.de', adress: 'Sonnengasse 3, 38304 Wolfenbüttel', gams_id: 'c1')
Customer.create!(namela: 'Lustig', namefi: 'Alfred', email: 'alfred.lustig@web.de', adress: 'Lärchenweg 15, 89073 Ulm', gams_id: 'c2')
Customer.create!(namela: 'Müller', namefi: 'Helmut', email: 'helmut.mueller@web.de', adress: 'Lindenstraße 27, 30625 Hannover', gams_id: 'c3')
Customer.create!(namela: 'Brockhaus', namefi: 'Elisabeth', email: 'elisabeth.brockhaus@gmx.de', adress: 'Hauptstraße 8, 44137 Dortmund', gams_id: 'c4')

# Role-Assignments Customers
Assignment.create!(user_id: 3, role_id: 2)
Assignment.create!(user_id: 4, role_id: 2)
Assignment.create!(user_id: 5, role_id: 2)
Assignment.create!(user_id: 6, role_id: 2)

# Supplier
Supplier.create!(name: 'Total', contact_firstname: 'Klara', contact_lastname: 'Winterscheit', email: 'winterscheit@total.com', gams_id: 's1')
Supplier.create!(name: 'BP', contact_firstname: 'Herbert', contact_lastname: 'Köhler', email: 'koehler@bp.com', gams_id: 's2')
Supplier.create!(name: 'Shell', contact_firstname: 'Christian', contact_lastname: 'Martens', email: 'martens@shell.com', gams_id: 's3')

# Role-Assignments Supplier
Assignment.create!(user_id: 7, role_id: 3)
Assignment.create!(user_id: 8, role_id: 3)
Assignment.create!(user_id: 9, role_id: 3)

# Final Product
FinalProduct.create!(name: "Endprodukt", price: 150)

# Months
Month.create!(name: "m1", description: "Januar")
Month.create!(name: "m2", description: "Februar")
Month.create!(name: "m3", description: "März")
Month.create!(name: "m4", description: "April")
Month.create!(name: "m5", description: "Mai")
Month.create!(name: "m6", description: "Juni")

# Orders
Order.create!(customer_id: 1, final_product_id: 1, month_id: 1, quantity: 80)
Order.create!(customer_id: 1, final_product_id: 1, month_id: 2, quantity: 30)
Order.create!(customer_id: 1, final_product_id: 1, month_id: 3, quantity: 30)
Order.create!(customer_id: 1, final_product_id: 1, month_id: 4, quantity: 60)
Order.create!(customer_id: 1, final_product_id: 1, month_id: 5, quantity: 80)
Order.create!(customer_id: 1, final_product_id: 1, month_id: 6, quantity: 30)

Order.create!(customer_id: 2, final_product_id: 1, month_id: 1, quantity: 80)
Order.create!(customer_id: 2, final_product_id: 1, month_id: 2, quantity: 70)
Order.create!(customer_id: 2, final_product_id: 1, month_id: 3, quantity: 100)
Order.create!(customer_id: 2, final_product_id: 1, month_id: 4, quantity: 100)
Order.create!(customer_id: 2, final_product_id: 1, month_id: 5, quantity: 60)
Order.create!(customer_id: 2, final_product_id: 1, month_id: 6, quantity: 110)

Order.create!(customer_id: 3, final_product_id: 1, month_id: 1, quantity: 130)
Order.create!(customer_id: 3, final_product_id: 1, month_id: 2, quantity: 80)
Order.create!(customer_id: 3, final_product_id: 1, month_id: 3, quantity: 120)
Order.create!(customer_id: 3, final_product_id: 1, month_id: 4, quantity: 100)
Order.create!(customer_id: 3, final_product_id: 1, month_id: 5, quantity: 100)
Order.create!(customer_id: 3, final_product_id: 1, month_id: 6, quantity: 140)

Order.create!(customer_id: 4, final_product_id: 1, month_id: 1, quantity: 140)
Order.create!(customer_id: 4, final_product_id: 1, month_id: 2, quantity: 130)
Order.create!(customer_id: 4, final_product_id: 1, month_id: 3, quantity: 140)
Order.create!(customer_id: 4, final_product_id: 1, month_id: 4, quantity: 150)
Order.create!(customer_id: 4, final_product_id: 1, month_id: 5, quantity: 160)
Order.create!(customer_id: 4, final_product_id: 1, month_id: 6, quantity: 130)

# Warehousings
(1..5).each do |n|
  Warehousing.create!(rawoil_id: n, storage_id: n, month_id: 1)
  Warehousing.create!(rawoil_id: n, storage_id: n, month_id: 2)
  Warehousing.create!(rawoil_id: n, storage_id: n, month_id: 3)
  Warehousing.create!(rawoil_id: n, storage_id: n, month_id: 4)
  Warehousing.create!(rawoil_id: n, storage_id: n, month_id: 5)
  Warehousing.create!(rawoil_id: n, storage_id: n, month_id: 6)
end

# Prices - Supplier
# Supplier 1
Shipment.create!(supplier_id: 1, rawoil_id: 1, month_id: 1, price: 100)
Shipment.create!(supplier_id: 1, rawoil_id: 2, month_id: 1, price: 120)
Shipment.create!(supplier_id: 1, rawoil_id: 3, month_id: 1, price: 110)
Shipment.create!(supplier_id: 1, rawoil_id: 4, month_id: 1, price: 150)
Shipment.create!(supplier_id: 1, rawoil_id: 5, month_id: 1, price: 140)

Shipment.create!(supplier_id: 1, rawoil_id: 1, month_id: 2, price: 120)
Shipment.create!(supplier_id: 1, rawoil_id: 2, month_id: 2, price: 140)
Shipment.create!(supplier_id: 1, rawoil_id: 3, month_id: 2, price: 90)
Shipment.create!(supplier_id: 1, rawoil_id: 4, month_id: 2, price: 130)
Shipment.create!(supplier_id: 1, rawoil_id: 5, month_id: 2, price: 120)

Shipment.create!(supplier_id: 1, rawoil_id: 1, month_id: 3, price: 130)
Shipment.create!(supplier_id: 1, rawoil_id: 2, month_id: 3, price: 100)
Shipment.create!(supplier_id: 1, rawoil_id: 3, month_id: 3, price: 120)
Shipment.create!(supplier_id: 1, rawoil_id: 4, month_id: 3, price: 180)
Shipment.create!(supplier_id: 1, rawoil_id: 5, month_id: 3, price: 110)

Shipment.create!(supplier_id: 1, rawoil_id: 1, month_id: 4, price: 90)
Shipment.create!(supplier_id: 1, rawoil_id: 2, month_id: 4, price: 100)
Shipment.create!(supplier_id: 1, rawoil_id: 3, month_id: 4, price: 150)
Shipment.create!(supplier_id: 1, rawoil_id: 4, month_id: 4, price: 140)
Shipment.create!(supplier_id: 1, rawoil_id: 5, month_id: 4, price: 120)

Shipment.create!(supplier_id: 1, rawoil_id: 1, month_id: 5, price: 120)
Shipment.create!(supplier_id: 1, rawoil_id: 2, month_id: 5, price: 140)
Shipment.create!(supplier_id: 1, rawoil_id: 3, month_id: 5, price: 100)
Shipment.create!(supplier_id: 1, rawoil_id: 4, month_id: 5, price: 120)
Shipment.create!(supplier_id: 1, rawoil_id: 5, month_id: 5, price: 160)

Shipment.create!(supplier_id: 1, rawoil_id: 1, month_id: 6, price: 100)
Shipment.create!(supplier_id: 1, rawoil_id: 2, month_id: 6, price: 115)
Shipment.create!(supplier_id: 1, rawoil_id: 3, month_id: 6, price: 150)
Shipment.create!(supplier_id: 1, rawoil_id: 4, month_id: 6, price: 80)
Shipment.create!(supplier_id: 1, rawoil_id: 5, month_id: 6, price: 130)

# Supplier 2
Shipment.create!(supplier_id: 2, rawoil_id: 1, month_id: 1, price: 110)
Shipment.create!(supplier_id: 2, rawoil_id: 2, month_id: 1, price: 140)
Shipment.create!(supplier_id: 2, rawoil_id: 3, month_id: 1, price: 100)
Shipment.create!(supplier_id: 2, rawoil_id: 4, month_id: 1, price: 140)
Shipment.create!(supplier_id: 2, rawoil_id: 5, month_id: 1, price: 110)

Shipment.create!(supplier_id: 2, rawoil_id: 1, month_id: 2, price: 130)
Shipment.create!(supplier_id: 2, rawoil_id: 2, month_id: 2, price: 170)
Shipment.create!(supplier_id: 2, rawoil_id: 3, month_id: 2, price: 90)
Shipment.create!(supplier_id: 2, rawoil_id: 4, month_id: 2, price: 110)
Shipment.create!(supplier_id: 2, rawoil_id: 5, month_id: 2, price: 130)

Shipment.create!(supplier_id: 2, rawoil_id: 1, month_id: 3, price: 110)
Shipment.create!(supplier_id: 2, rawoil_id: 2, month_id: 3, price: 140)
Shipment.create!(supplier_id: 2, rawoil_id: 3, month_id: 3, price: 100)
Shipment.create!(supplier_id: 2, rawoil_id: 4, month_id: 3, price: 150)
Shipment.create!(supplier_id: 2, rawoil_id: 5, month_id: 3, price: 140)

Shipment.create!(supplier_id: 2, rawoil_id: 1, month_id: 4, price: 100)
Shipment.create!(supplier_id: 2, rawoil_id: 2, month_id: 4, price: 120)
Shipment.create!(supplier_id: 2, rawoil_id: 3, month_id: 4, price: 90)
Shipment.create!(supplier_id: 2, rawoil_id: 4, month_id: 4, price: 170)
Shipment.create!(supplier_id: 2, rawoil_id: 5, month_id: 4, price: 120)

Shipment.create!(supplier_id: 2, rawoil_id: 1, month_id: 5, price: 110)
Shipment.create!(supplier_id: 2, rawoil_id: 2, month_id: 5, price: 140)
Shipment.create!(supplier_id: 2, rawoil_id: 3, month_id: 5, price: 130)
Shipment.create!(supplier_id: 2, rawoil_id: 4, month_id: 5, price: 140)
Shipment.create!(supplier_id: 2, rawoil_id: 5, month_id: 5, price: 130)

Shipment.create!(supplier_id: 2, rawoil_id: 1, month_id: 6, price: 110)
Shipment.create!(supplier_id: 2, rawoil_id: 2, month_id: 6, price: 190)
Shipment.create!(supplier_id: 2, rawoil_id: 3, month_id: 6, price: 100)
Shipment.create!(supplier_id: 2, rawoil_id: 4, month_id: 6, price: 90)
Shipment.create!(supplier_id: 2, rawoil_id: 5, month_id: 6, price: 100)

# Supplier 3
Shipment.create!(supplier_id: 3, rawoil_id: 1, month_id: 1, price: 90)
Shipment.create!(supplier_id: 3, rawoil_id: 2, month_id: 1, price: 130)
Shipment.create!(supplier_id: 3, rawoil_id: 3, month_id: 1, price: 80)
Shipment.create!(supplier_id: 3, rawoil_id: 4, month_id: 1, price: 110)
Shipment.create!(supplier_id: 3, rawoil_id: 5, month_id: 1, price: 100)

Shipment.create!(supplier_id: 3, rawoil_id: 1, month_id: 2, price: 130)
Shipment.create!(supplier_id: 3, rawoil_id: 2, month_id: 2, price: 110)
Shipment.create!(supplier_id: 3, rawoil_id: 3, month_id: 2, price: 120)
Shipment.create!(supplier_id: 3, rawoil_id: 4, month_id: 2, price: 95)
Shipment.create!(supplier_id: 3, rawoil_id: 5, month_id: 2, price: 150)

Shipment.create!(supplier_id: 3, rawoil_id: 1, month_id: 3, price: 110)
Shipment.create!(supplier_id: 3, rawoil_id: 2, month_id: 3, price: 110)
Shipment.create!(supplier_id: 3, rawoil_id: 3, month_id: 3, price: 75)
Shipment.create!(supplier_id: 3, rawoil_id: 4, month_id: 3, price: 110)
Shipment.create!(supplier_id: 3, rawoil_id: 5, month_id: 3, price: 120)

Shipment.create!(supplier_id: 3, rawoil_id: 1, month_id: 4, price: 100)
Shipment.create!(supplier_id: 3, rawoil_id: 2, month_id: 4, price: 900)
Shipment.create!(supplier_id: 3, rawoil_id: 3, month_id: 4, price: 120)
Shipment.create!(supplier_id: 3, rawoil_id: 4, month_id: 4, price: 130)
Shipment.create!(supplier_id: 3, rawoil_id: 5, month_id: 4, price: 170)

Shipment.create!(supplier_id: 3, rawoil_id: 1, month_id: 5, price: 140)
Shipment.create!(supplier_id: 3, rawoil_id: 2, month_id: 5, price: 130)
Shipment.create!(supplier_id: 3, rawoil_id: 3, month_id: 5, price: 80)
Shipment.create!(supplier_id: 3, rawoil_id: 4, month_id: 5, price: 130)
Shipment.create!(supplier_id: 3, rawoil_id: 5, month_id: 5, price: 100)

Shipment.create!(supplier_id: 3, rawoil_id: 1, month_id: 6, price: 90)
Shipment.create!(supplier_id: 3, rawoil_id: 2, month_id: 6, price: 80)
Shipment.create!(supplier_id: 3, rawoil_id: 3, month_id: 6, price: 160)
Shipment.create!(supplier_id: 3, rawoil_id: 4, month_id: 6, price: 140)
Shipment.create!(supplier_id: 3, rawoil_id: 5, month_id: 6, price: 120)


# Consumptions
(1..5).each do |n|
  Consumption.create!(rawoil_id: n, month_id: 1)
  Consumption.create!(rawoil_id: n, month_id: 2)
  Consumption.create!(rawoil_id: n, month_id: 3)
  Consumption.create!(rawoil_id: n, month_id: 4)
  Consumption.create!(rawoil_id: n, month_id: 5)
  Consumption.create!(rawoil_id: n, month_id: 6)
end

# Productions
(1..6).each do |n|
  Production.create!(month_id: n)
end

# Balance
Balance.create!(value: 0, status: 0)

#GAMS-Path
GamsPath.create!(gams_path_url: "C:\\GAMS\\win64\\24.5\\gams")

# Administration
Administration.create!(category: "Minimaler Verbrauch bei Verwendung eines Rohöhls", value: 20)
Administration.create!(category: "Maximale Anzahl an zu vermengenden Rohölen", value: 3)
Administration.create!(category: "Maximale Lagertankkapazität", value: 400)
Administration.create!(category: "Anfangs- und Endlagerbestand", value: 200)
Administration.create!(category: "Lagerkosten", value: 5)
Administration.create!(category: "Maximale Dichte", value: 0.845)
Administration.create!(category: "Minimale Dichte", value: 0.75)

# User-Roles
Role.create!([
                 { name: 'Admin', description: 'Admin' },
                 { name: 'Customer', description: 'Kunde' },
                 { name: 'Supplier', description: 'Lieferant' },
                 { name: 'Guest', description: 'Gast' }
             ])
