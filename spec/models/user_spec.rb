require 'rails_helper'

describe User do
  it 'has a valid factory' do
    expect(create(:user)).to be_valid
  end

  it 'is invalid without a firstname' do
    expect(build(:user, firstname: nil)).to_not be_valid
  end

  it 'is invalid without a lastname' do
    expect(build(:user, name: nil)).to_not be_valid
  end

  it 'is invalid without an email' do
    expect(build(:user, email: nil)).to_not be_valid
  end

  it 'returns the revenue' do
    user = create(:user)
    create(:finalProduct, price: 100)
    2.times { create(:production, production: 10) }

    expect(user.revenue).to eq(2000)
  end

  it 'returns the total storage costs' do
    user = create(:user)
    4.times { create(:administration) }
    create(:administration, value: 5)
    2.times { create(:warehousing, stock: 10) }

    expect(user.storage_cost).to eq(100)
  end

  it 'returns the total rawoil costs' do
    user = create(:user)
    create(:shipment, supplier_id: user.id, month_id: 1, price: 50, quantity: 10)
    create(:shipment, supplier_id: user.id, month_id: 4, price: 50, quantity: 10)

    expect(user.cost).to eq(1000)
  end

  it 'returns the profit gained' do
    user = create(:user)
    create(:balance, value: 1000)

    expect(user.profit).to eq(1000)
  end

  it 'returns the size of all orders by a particular customer' do
    user = create(:user)
    create(:customer, namefi: user.firstname, namela: user.name)
    create(:order, customer_id: user.id, month_id: 1, quantity: 1000)
    create(:order, customer_id: user.id, month_id: 2, quantity: 1000)

    expect(user.order_size(user)).to eq(2000)
  end

  it 'returns the sum a particular customer has to pay' do
    user = create(:user)
    create(:customer, namefi: user.firstname, namela: user.name)
    create(:order, customer_id: user.id, month_id: 1, quantity: 20)
    create(:order, customer_id: user.id, month_id: 3, quantity: 40)
    create(:finalProduct, price: 150)

    expect(user.estimated_price(user)).to eq(9000)
  end

  it 'returns the amount of rawoils purchased by a particular supplier' do
    user = create(:user)
    rawoil = create(:rawoil, :pv)
    create(:supplier, contact_firstname: user.firstname, contact_lastname: user.name)
    create(:shipment, supplier_id: user.id, month_id: 1, rawoil_id: rawoil.id, price: 20, quantity: 30)
    create(:shipment, supplier_id: user.id, month_id: 6, rawoil_id: rawoil.id, price: 20, quantity: 30)

    expect(user.rawoil_purchases(user, rawoil)).to eq(1200)
  end


  it 'returns the profit of a particular supplier due to rawoil sale' do
    user = create(:user)
    create(:supplier, contact_firstname: user.firstname, contact_lastname: user.name)
    create(:shipment, supplier_id: user.id, month_id: 1, price: 50, quantity: 10)
    create(:shipment, supplier_id: user.id, month_id: 5, price: 50, quantity: 10)

    expect(user.shipment_price(user)).to eq(1000)
  end
end