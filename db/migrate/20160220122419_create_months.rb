class CreateMonths < ActiveRecord::Migration
  def change
    create_table :months do |t|
      t.string :name
      t.string :description

      t.timestamps null: false
    end
  end
end
