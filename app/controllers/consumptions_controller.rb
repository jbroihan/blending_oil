class ConsumptionsController < ApplicationController
  load_and_authorize_resource
  before_action :set_consumption, only: [:show]

  # GET /consumptions
  # GET /consumptions.json
  def index
    @consumptions = Consumption.all

    respond_to do |format|
      format.html
      format.xls { send_data(@consumptions.to_xls) }
      format.xls {
        filename = "Consumptions-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@consumptions.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /consumptions/1
  # GET /consumptions/1.json
  def show
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_consumption
    @consumption = Consumption.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def consumption_params
    params.require(:consumption).permit(:month_id, :rawoil_id, :induse, :use)
  end
end
