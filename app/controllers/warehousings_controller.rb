class WarehousingsController < ApplicationController
  load_and_authorize_resource
  before_action :set_warehousing, only: [:show]

  # GET /warehousings
  # GET /warehousings.json
  def index
    @warehousings = Warehousing.all

    respond_to do |format|
      format.html
      format.xls { send_data(@warehousings.to_xls) }
      format.xls {
        filename = "Warehousings-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@warehousings.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /warehousings/1
  # GET /warehousings/1.json
  def show
  end

  def optimize
    # Delete old include file
    if File.file?("Oil_include.inc")
      File.delete("Oil_include.inc")
    end
    # New include file
    f = File.new("Oil_include.inc", "w")

    # Sets
    printf(f, "Sets \n \t m \t\t\t/ " + Month.first.name + "*" + Month.last.name + " / \n") #Set Monate
    printf(f, "\t p \t\t\t/ " + Rawoil.first.name + "*" + Rawoil.last.name + " / \n") #Set Rohoele

    printf(f, "\t c \t\t\t/ " + Customer.first.gams_id + "*" + Customer.last.gams_id + " / \n") #Set Kunden
    printf(f, "\t s \t\t\t/ " +Supplier.first.gams_id + "*" + Supplier.last.gams_id + " / \n") #Set Lieferanten

    printf(f, "\t mpa \t\t/ " + "mpa" + Warehousing.first.id.to_s + "*" + "mpa" + Warehousing.last.id.to_s + " / \n") #Rohöl-Monat-Assoziation
    printf(f, "\t mpsa \t\t/ " + "mpsa" + Shipment.first.id.to_s + "*" + "mpsa" + Shipment.last.id.to_s + " / \n") #Rohöl-Monat-Lieferung-Assoziationen mpsa

    printf(f, "\t mpoa \t\t/ " + "mpoa" + Consumption.first.id.to_s + "*" + "mpoa" + Consumption.last.id.to_s + " / \n") #Monat-Produktionsmengen-Assoziation
    printf(f, "\t mproda \t/ " + "mproda" + Production.first.id.to_s + "*" + "mproda" + Production.last.id.to_s + " / \n") #Monat-Produktionsmengen-Assoziationen mprod

    printf(f, "\t pv(p) \t \t/ ")
    @rawoil_pv = Rawoil.where(category: 'PV')
    @rawoil_pv.each do |oil|
      printf(f, "\n \t\t\t\t\t" + oil.name)
    end
    printf(f, " / \n")

    printf(f, "\t pnv(p) \t / ")
    @rawoil_pnv = Rawoil.where(category: 'PNV')
    @rawoil_pnv.each do |oil|
      printf(f, "\n \t\t\t\t\t" + oil.name)
    end
    printf(f, " /; \n \n \n")


    # Parameter
    printf(f, "Parameters \n \t maxstore \t / " + Administration.find(3).value.to_s + " / \n") #Maximum capacity of rawoil storages
    printf(f, "\t maxusepv \t / " + Refinery.find(1).capacity.to_s + "  / \n") #Maximum capacity of pv refinery
    printf(f, "\t maxusepnv \t / " + Refinery.find(2).capacity.to_s + "  / \n") #Maxmimum capacity of pnv refinery
    printf(f, "\t minusep \t / " + Administration.find(1).value.to_s + " / \n") #Minimum use of rawoil if used
    printf(f, "\t maxnusep \t / " + Administration.find(2).value.to_s + " / \n") #Maximum number of oils used in blend
    printf(f, "\t sp \t\t / " + FinalProduct.find(1).price.to_s + " / \n") #Sales price of final product
    printf(f, "\t sc \t\t / " + Administration.find(5).value.to_s + " / \n") #Storage costs
    printf(f, "\t stock(p) \t / \#p " + Administration.find(4).value.to_s + " / \n") #Stock at begin and end
    printf(f, "\t hmin \t\t / " + Administration.find(7).value.to_s + " / \n") #Minumum hardness
    printf(f, "\t hmax \t\t / " + Administration.find(6).value.to_s + " / \n") #Maximum hardness

    printf(f, "\t h(p) \t\t / ")
    @rawoil = Rawoil.all
    @rawoil.each do |oil|
      printf(f, "\n \t\t\t\t\t" + oil.name + " " + oil.density.to_s)
    end
    printf(f, " /; \n\n")

    printf(f, "\n MP(m,p) = yes;")
    printf(f, "\n MPS(m,p,s) = yes;")
    printf(f, "\n MPO(m,p) = yes;")
    printf(f, "\n MPROD(m) = yes; \n\n\n")

    # orders
    @order = Order.all
    @order.each do |order|
      printf(f, "demand('" + order.month.name + "','" + order.customer.gams_id + "') = " + order.quantity.to_s + ";\n")
    end

    printf(f, "\n\n")

    # prices
    @shipment = Shipment.all
    @shipment.each do |ship|
      printf(f, "cost('" + ship.month.name + "','" + ship.rawoil.name + "','" + ship.supplier.gams_id + "') = " +
          ship.price.to_s + ";\n")
    end

    printf(f, "\n\n MPaMP(mpa,m,p) = no;\n\n")
    @rawoil_month_association = Warehousing.all
    @rawoil_month_association.each do |rawoil_month|
      printf(f, "MPaMP('mpa" + rawoil_month.id.to_s + "','" + rawoil_month.month.name + "','" +
          rawoil_month.rawoil.name + "') = yes; \n")
    end
    printf(f, "\n\n")

    printf(f, "MPSaMPS(mpsa,m,p,s) = no; \n \n")
    @rawoil_month_shipment_association = Shipment.all
    @rawoil_month_shipment_association.each do |rawoil_month_shipment|
      printf(f, "MPSaMPS('mpsa" + rawoil_month_shipment.id.to_s + "','" + rawoil_month_shipment.month.name + "','" +
          rawoil_month_shipment.rawoil.name + "','" + rawoil_month_shipment.supplier.gams_id + "') = yes; \n")
    end
    printf(f, "\n\n")

    printf(f, "MPOaMPO(mpoa,m,p) = no; \n \n")
    @rawoil_month_use_association = Consumption.all
    @rawoil_month_use_association.each do |rawoil_month_use|
      printf(f, "MPOaMPO('mpoa" + rawoil_month_use.id.to_s + "','" + rawoil_month_use.month.name + "','" +
          rawoil_month_use.rawoil.name + "') = yes; \n")
    end
    printf(f, "\n\n")

    printf(f, "MPRODaMPROD(mproda,m) = no; \n \n")
    @month_production_association = Production.all
    @month_production_association.each do |month_production_association|
      printf(f, "MPRODaMPROD('mproda" + month_production_association.id.to_s + "','" +
          month_production_association.month.name + "') = yes; \n")
    end
    printf(f, "\n\n")

    f.close

    if File.file?("Blending_of_Oil_solution_profit.txt")
      File.delete("Blending_of_Oil_solution_profit.txt")
    end

    system GamsPath.find(1).gams_path_url + " Oil_Modell_modified.gms"

    flash.now[:started] = "Die Rechnung wurde gestartet!"

    if (File.exist? "Blending_of_Oil_solution_store.txt")
      fi=File.open("Blending_of_Oil_solution_store.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "mpa"
        sa3=sa[3].delete "\n"
        rawoil_month_association = Warehousing.find_by_id(sa0)
        rawoil_month_association.stock = sa3
        rawoil_month_association.save
      }
      fi.close

    end

    if (File.exist? "Blending_of_Oil_solution_buy.txt")
      fi=File.open("Blending_of_Oil_solution_buy.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "mpsa"
        sa4=sa[4].delete "\n"
        rawoil_month_shipment_association = Shipment.find_by_id(sa0)
        rawoil_month_shipment_association.quantity = sa4
        rawoil_month_shipment_association.save
      }
      fi.close
    end

    if (File.exist? "Blending_of_Oil_solution_use.txt")
      fi=File.open("Blending_of_Oil_solution_use.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "mpoa"
        sa3=sa[3]
        sa4=sa[4].delete "\n"
        rawoil_month_use_association = Consumption.find_by_id(sa0)
        rawoil_month_use_association.use = sa3
        rawoil_month_use_association.induse = sa4
        rawoil_month_use_association.save
      }
      fi.close
    end

    if (File.exist? "Blending_of_Oil_solution_produce.txt")
      fi=File.open("Blending_of_Oil_solution_produce.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "mproda"
        sa2=sa[2].delete "\n"
        month_production_association = Production.find_by_id(sa0)
        month_production_association.production = sa2
        month_production_association.save
      }
      fi.close

      # objective function value
      if File.exist?("Blending_of_Oil_solution_profit.txt")
        fi=File.open("Blending_of_Oil_solution_profit.txt", "r")
        line=fi.readline
        fi.close
        sa=line.split(" ")
        @objective_function_value = sa[1]
        ofv = Balance.first
        ofv.status = sa[3]
        ofv.value = @objective_function_value
        ofv.save
        @modelstatus = Balance.first.status

      else
        @objective_function_vaue = nil
        flash.now[:not_available] = "Zielfunktionswert wurde noch nicht berechnet."
      end

      # calculation
      @productions = Production.all
      @productions.each do |production|
        production.excess_demand = Order.where(month_id: production.month_id).sum(:quantity) - production.production
        production.save
      end

      # Render
      render :template => "productions/index"

    end
  end

  def delete_old_plan
    if File.exist?("Blending_of_Oil_solution_store.txt")
      File.delete("Blending_of_Oil_solution_store.txt")
    end

    if File.exist?("Blending_of_Oil_solution_buy.txt")
      File.delete("Blending_of_Oil_solution_buy.txt")
    end

    if File.exist?("Blending_of_Oil_solution_profit.txt")
      File.delete("Blending_of_Oil_solution_profit.txt")
    end

    if File.exist?("Blending_of_Oil_solution_use.txt")
      File.delete("Blending_of_Oil_solution_use.txt")
    end

    if File.exist?("Blending_of_Oil_solution_produce.txt")
      File.delete("Blending_of_Oil_solution_produce.txt")
    end

    @warehousings = Warehousing.all
    @warehousings.each do |rawoil_month|
      rawoil_month.stock = nil
      rawoil_month.save
    end

    @rawoil_month_shipment_association=Shipment.all
    @rawoil_month_shipment_association.each do |rawoil_month_shipment|
      rawoil_month_shipment.quantity = nil
      rawoil_month_shipment.save
    end

    @rawoil_month_use_association = Consumption.all
    @rawoil_month_use_association.each do |rawoil_month_use|
      rawoil_month_use.induse = nil
      rawoil_month_use.use = nil
      rawoil_month_use.save
    end

    @month_production_association = Production.all
    @month_production_association.each do |month_production_association|
      month_production_association.production = nil
      month_production_association.excess_demand = nil
      month_production_association.save
    end

    @objective_function_value = Balance.all
    @objective_function_value.each do |ofv|
      ofv.value = 0
      ofv.status =
          ofv.save
    end

    render 'static_pages/blending_start'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_warehousing
    @warehousing = Warehousing.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def warehousing_params
    params.require(:warehousing).permit(:rawoil_id, :storage_id, :month_id, :stock)
  end
end