class CreateStorages < ActiveRecord::Migration
  def change
    create_table :storages do |t|
      t.string :name
      t.float :capacity

      t.timestamps null: false
    end
  end
end
