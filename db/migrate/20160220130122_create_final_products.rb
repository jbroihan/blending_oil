class CreateFinalProducts < ActiveRecord::Migration
  def change
    create_table :final_products do |t|
      t.string :name
      t.float :price

      t.timestamps null: false
    end
  end
end
