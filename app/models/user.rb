class User < ActiveRecord::Base

  has_many :assignments
  has_many :roles, through: :assignments


  before_save { self.email = email.downcase }
  validates :name, :firstname, presence: true, length: {maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true


  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns the revenue gained due to selling final products
  def revenue
    FinalProduct.first.price * Production.all.sum(:production)
  end

  # Returns total storage costs
  def storage_cost
    Administration.fifth.value * Warehousing.all.sum(:stock)
  end

  # Returns the total cost of all rawoil purchases
  def cost
    Shipment.all.sum('price * quantity')
  end

  # Returns the objective function value
  def profit
    Balance.first.value
  end

  # Returns the size of all orders by a particular customer
  def order_size(user)
    customer = Customer.where(customers: {namefi: user.firstname, namela: user.name})
    orders = Order.where(customer_id: customer.pluck(:id))
    orders.sum(:quantity)
  end

  # Returns the sum a particular customer has to pay
  def estimated_price(user)
    customer = Customer.where(customers: {namefi: user.firstname, namela: user.name})
    orders = Order.where(customer_id: customer.pluck(:id))
    orders.sum(:quantity) * FinalProduct.first.price
  end

  # Return the amount of rawoils purchased by a particular supplier
  def rawoil_purchases(user, rawoil)
    supplier = Supplier.where(suppliers: {contact_firstname: user.firstname, contact_lastname: user.name})
    rawoil.shipments.where(supplier_id: supplier.pluck(:id)).sum('price * quantity')
  end

  # Returns the profit of a particular supplier due to rawoil sale
  def shipment_price(user)
    supplier = Supplier.where(suppliers: {contact_firstname: user.firstname, contact_lastname: user.name})
    shipment = Shipment.where(supplier_id: supplier.pluck(:id))
    shipment.sum('price * quantity')
  end
end
