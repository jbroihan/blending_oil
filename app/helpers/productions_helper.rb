module ProductionsHelper
  def prod_chart_data
    data = [
        {
            name: 'Produktionsmengen',
            data: Production.group(:month_id).sum(:production)
        },
        {
            name: 'Nachfrageüberschuss',
            data: Production.group(:month_id).sum(:excess_demand)
        }
    ]
  end
end
