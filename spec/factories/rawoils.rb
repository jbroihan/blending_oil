require 'faker'

FactoryGirl.define do
  factory :rawoil do
    name      'p1'
    density   { Faker::Number.between(0.1, 1.2) }

    trait :pv do
              category 'PV'
    end

    trait :pnv do
              category 'PNV'
    end
  end
end