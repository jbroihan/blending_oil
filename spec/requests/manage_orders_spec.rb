require 'rails_helper'

RSpec.configure do |c|
  c.include SessionHelpers
end

feature 'Managing orders' do
  before do
    @user = create(:user)
    create(:role, name: 'Admin')
    create(:role, name: 'Customer')
    create(:role, name: 'Supplier')
    create(:month, description: 'Januar') # in order to be able to select month in dropdown
    @final_product = create(:finalProduct) # in order to be able to select in dropdown
  end

  scenario 'Admin creates a new order and gets notified' do
    customer = create(:customer) # in order to be able to select customer in dropdown
    create(:assignment, user_id: @user.id, role_id: 1)
    sign_in(@user)

    visit new_order_path
    select customer.fullname, from: 'Kunde'
    select @final_product.name, from: 'Endprodukt'
    select 'Januar', from: 'Monat'
    fill_in 'Menge', with: 110
    click_on 'bestätigen'

    expect(page).to have_content('erfolgreich')
  end

  scenario 'Admin updates an order and gets notified' do
    create(:assignment, user_id: @user.id, role_id: 1)
    customer = create(:customer)
    order = create(:order, customer_id: customer.id) # to be able to edit it
    sign_in(@user)

    visit edit_order_path(order)

    fill_in 'Menge', with: 100
    click_on 'bestätigen'

    expect(page).to have_content('erfolgreich')
  end

  scenario 'Customer has access to orders' do
    create(:customer, namefi: @user.firstname, namela: @user.name, email: @user.email)
    create(:assignment, user_id: @user.id, role_id: 2)
    sign_in(@user)

    visit orders_path
    expect(page).to have_content('Bestellungen')
  end

  scenario 'Supplier has no authorized access to orders' do
    create(:supplier, contact_firstname: @user.firstname, contact_lastname: @user.name, email: @user.email)
    create(:assignment, user_id: @user.id, role_id: 3)
    sign_in(@user)

    visit orders_path
    expect(page).to have_content('Zugriff auf diese Seite verweigert')
  end

end