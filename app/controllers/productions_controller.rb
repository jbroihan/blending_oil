class ProductionsController < ApplicationController
  load_and_authorize_resource
  before_action :set_production, only: [:show]

  # GET /productions
  # GET /productions.json
  def index
    @productions = Production.all
    @objective_function_value = Balance.first.value
    @modelstatus = Balance.first.status

    respond_to do |format|
      format.html
      format.xls { send_data(@productions.to_xls) }
      format.xls {
        filename = "Productions-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@productions.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /productions/1
  # GET /productions/1.json
  def show
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_production
    @production = Production.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def production_params
    params.require(:production).permit(:month_id, :production)
  end
end
