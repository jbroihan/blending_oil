# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160317115401) do

  create_table "administrations", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float    "value"
  end

  create_table "assignments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "balances", force: :cascade do |t|
    t.float    "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float    "status"
  end

  create_table "consumptions", force: :cascade do |t|
    t.integer  "month_id"
    t.integer  "rawoil_id"
    t.binary   "induse"
    t.float    "use"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "namefi"
    t.string   "namela"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "gams_id"
    t.string   "adress"
  end

  create_table "demandsites", force: :cascade do |t|
    t.integer  "site_id"
    t.float    "demand_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "final_products", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gams_paths", force: :cascade do |t|
    t.string   "gams_path_url"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "machine_period_associations", force: :cascade do |t|
    t.integer  "machine_id"
    t.integer  "period_id"
    t.float    "capacity"
    t.float    "overtime"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "machines", force: :cascade do |t|
    t.string   "name"
    t.float    "overtime_cost"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "months", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "final_product_id"
    t.integer  "month_id"
    t.float    "quantity"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "periods", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_period_associations", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "period_id"
    t.float    "demand"
    t.float    "production_quantity"
    t.float    "inventory_level"
    t.integer  "setup_variable"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "product_product_associations", force: :cascade do |t|
    t.integer  "predecessor_id"
    t.integer  "successor_id"
    t.float    "coefficient"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "productions", force: :cascade do |t|
    t.integer  "month_id"
    t.float    "production"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.float    "excess_demand"
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.float    "setup_time"
    t.float    "processing_time"
    t.float    "setup_cost"
    t.float    "holding_cost"
    t.float    "initial_inventory"
    t.integer  "lead_time_periods"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "machine_id"
  end

  create_table "rawoils", force: :cascade do |t|
    t.string   "name"
    t.float    "density"
    t.string   "category"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "refinery_id"
    t.integer  "final_product_id"
  end

  create_table "refineries", force: :cascade do |t|
    t.string   "name"
    t.float    "capacity"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "description"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "description"
  end

  create_table "shipments", force: :cascade do |t|
    t.integer  "supplier_id"
    t.integer  "rawoil_id"
    t.integer  "month_id"
    t.float    "quantity"
    t.float    "price"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "sites", force: :cascade do |t|
    t.string   "name"
    t.string   "codename",   limit: 3
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "storages", force: :cascade do |t|
    t.string   "name"
    t.float    "capacity"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "description"
    t.integer  "rawoil_id"
  end

  create_table "suppliers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "gams_id"
    t.string   "contact_firstname"
    t.string   "contact_lastname"
  end

  create_table "supplysites", force: :cascade do |t|
    t.integer  "site_id"
    t.float    "supply_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "translinks", force: :cascade do |t|
    t.integer  "supplysite_id"
    t.integer  "demandsite_id"
    t.float    "unit_cost"
    t.float    "transport_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.string   "firstname"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

  create_table "warehousings", force: :cascade do |t|
    t.integer  "rawoil_id"
    t.integer  "storage_id"
    t.string   "month_id"
    t.float    "stock"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
