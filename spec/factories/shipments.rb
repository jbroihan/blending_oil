require 'faker'

FactoryGirl.define do
  factory :shipment do
    supplier_id       { 1 }
    rawoil_id         { Faker::Number.number(1) }
    month_id          { Faker::Number.between(1, 6)}
    quantity          { Faker::Number.number(2) }
    price             { Faker::Commerce.price }
  end
end