json.array!(@shipments) do |shipment|
  json.extract! shipment, :id, :supplier_id, :rawoil_id, :month_id, :quantity, :price
  json.url shipment_url(shipment, format: :json)
end
