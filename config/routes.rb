Rails.application.routes.draw do

  resources :administrations
  resources :gams_paths
  resources :productions
  resources :consumptions
  resources :warehousings
  resources :orders
  resources :shipments
  resources :final_products
  resources :storages
  resources :months
  resources :suppliers
  resources :customers
  resources :rawoils
  resources :refineries

  get 'blending_start' => 'static_pages#blending_start'
  get 'customer_signup' => 'custom_customer#new_customer'
  get 'supplier_signup' => 'custom_supplier#new_supplier'
  post 'warehousings/read_and_show_ofv', :to => 'warehousings#read_and_show_ofv'
  post 'warehousings/read_optimization_results', :to => 'warehousings#read_optimization_results'
  post 'warehousings/optimize', :to => 'warehousings#optimize'
  post 'warehousings/delete_old_plan', :to => 'warehousings#delete_old_plan'
  post 'warehousings/show_index_page', :to => 'warehousings#show_index_page'



  post "periods/change", as: :change_periods

  resources :machine_period_associations
  resources :periods
  resources :product_period_associations
  resources :product_product_associations
  resources :machines
  resources :items
  root             'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]

  resources :translinks
  resources :demandsites
  resources :supplysites
  resources :sites
  resources :products


  get 'transport_start', to: 'static_pages#transport_start'
  get 'mlclsp_start', to: 'static_pages#mlclsp_start'


  post 'translinks/read_and_show_ofv', :to => 'translinks#read_and_show_ofv'
  post 'translinks/read_transportation_quantities', :to => 'translinks#read_transportation_quantities'
  post 'translinks/optimize', :to => 'translinks#optimize'
  post 'translinks/delete_transportation_quantities', :to => 'translinks#delete_transportation_quantities'

  post 'product_periods/read_and_show_ofv', :to => 'product_period_associations#read_and_show_ofv'
  post 'product_periods/read_optimization_results', :to => 'product_period_associations#read_optimization_results'
  post 'product_periods/optimize', :to => 'product_period_associations#optimize'
  post 'product_periods/delete_old_plan', :to => 'product_period_associations#delete_old_plan'
  post 'product_periods/show_index_page', :to => 'product_period_associations#show_index_page'



end
