require 'test_helper'

class ProductProductAssociationsControllerTest < ActionController::TestCase
  setup do
    @product_product_association = product_product_associations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_product_associations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_product_association" do
    assert_difference('ProductProductAssociation.count') do
      post :create, product_product_association: { coefficient: @product_product_association.coefficient, predecessor_id: @product_product_association.predecessor_id, successor_id: @product_product_association.successor_id }
    end

    assert_redirected_to product_product_association_path(assigns(:product_product_association))
  end

  test "should show product_product_association" do
    get :show, id: @product_product_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_product_association
    assert_response :success
  end

  test "should update product_product_association" do
    patch :update, id: @product_product_association, product_product_association: { coefficient: @product_product_association.coefficient, predecessor_id: @product_product_association.predecessor_id, successor_id: @product_product_association.successor_id }
    assert_redirected_to product_product_association_path(assigns(:product_product_association))
  end

  test "should destroy product_product_association" do
    assert_difference('ProductProductAssociation.count', -1) do
      delete :destroy, id: @product_product_association
    end

    assert_redirected_to product_product_associations_path
  end
end
