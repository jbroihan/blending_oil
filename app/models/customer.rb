class Customer < ActiveRecord::Base
  validates :namefi, :namela, :email, :adress, :gams_id, presence: true
  validates_associated :orders
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }

  has_many :orders, :dependent => :destroy
  has_many :months, :through => :orders
  has_many :final_products, :through => :orders

  accepts_nested_attributes_for :orders, allow_destroy: true

  # Returns fullname of customer
  def fullname
    "#{namefi} #{namela}"
  end

end
