class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.integer :final_product_id
      t.integer :month_id
      t.float :quantity

      t.timestamps null: false
    end
  end
end
