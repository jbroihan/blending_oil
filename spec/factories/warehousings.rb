require 'faker'

FactoryGirl.define do
  factory :warehousing do
    month_id    { Faker::Number.between(1, 6) }
    rawoil_id   { Faker::Number.number(1) }
    storage_id  { Faker::Number.number(1) }
    stock       { Faker::Number.positive }
  end
end