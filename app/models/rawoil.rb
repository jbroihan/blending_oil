class Rawoil < ActiveRecord::Base
  validates :category, :density, presence: true
  validates :density, numericality: { greater_than_or_equal_to: 0 }

  belongs_to :refinery # 1:n Refinery and Rawoil
  belongs_to :final_product # 1:n Final Product and Rawoil

  has_many :consumptions, :dependent => :destroy
  has_many :months, :through => :consumptions

  # n:m Rawoil, Supplier and Month
  has_many :shipments, :dependent => :destroy
  has_many :suppliers, :through => :shipments
  has_many :months, :through => :shipments

  # n:m Rawoil, Storage and Month
  has_many :warehousings , :dependent => :destroy
  has_one :storage, dependent: :destroy
  has_many :months, :through => :warehousings
end
