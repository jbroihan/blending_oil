require 'rails_helper'
require 'cancan/matchers'

describe 'User' do
  describe 'abilities' do
    subject(:ability) { Ability.new(user) }
    let(:user) { nil }

    context 'when is an admin' do
      let(:user) { create(:user) }
      before do
        create(:role, name: 'Admin')
        create(:assignment, user_id: user.id, role_id: 1)
      end
      it 'should be able to index all' do
        should be_able_to(:index, :all)
      end
      it 'should be able to show all' do
        should be_able_to(:show, :all)
      end
      it 'should be able to edit all' do
        should be_able_to(:edit, :all)
      end
      it 'should be able to update all' do
        should be_able_to(:update, :all)
      end
      it 'should be able to create all' do
        should be_able_to(:create, :all)
      end
      it 'should be able to destroy all' do
        should be_able_to(:destroy, :all)
      end
      it 'should be able to use optimize on warehousing' do
        should be_able_to(:optimize, Warehousing)
      end
      it 'should be able to use delete_old_plan on warehousing' do
        should be_able_to(:delete_old_plan, Warehousing)
      end
    end

    context 'when is an customer' do
      let(:user) { create(:user) }
      before do
        create(:role, name: 'Admin')
        create(:role, name: 'Customer')
        create(:assignment, user_id: user.id, role_id: 2)
      end
      it 'should be able to index Final_Products, Customer, Order' do
        should be_able_to(:index, FinalProduct)
        should be_able_to(:index, Customer)
        should be_able_to(:index, Order)
      end
      it 'should be able to edit, show, edit, update and destroy User' do
        should be_able_to(:show, User)
        should be_able_to(:edit, User)
        should be_able_to(:update, User)
        should be_able_to(:destroy, User)
      end
    end

    context 'when is an supplier' do
      let(:user) { create(:user) }
      before do
        create(:role, name: 'Admin')
        create(:role, name: 'Customer')
        create(:role, name: 'Supplier')
        create(:assignment, user_id: user.id, role_id: 3)
      end
      it 'should be able to index Supplier and Shipments' do
        should be_able_to(:index, Supplier)
        should be_able_to(:index, Shipment)
      end
      it 'should be able to edit, show, edit, update and destroy User' do
        should be_able_to(:show, User)
        should be_able_to(:edit, User)
        should be_able_to(:update, User)
        should be_able_to(:destroy, User)
      end
    end
  end
end