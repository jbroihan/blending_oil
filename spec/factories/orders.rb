require 'faker'

FactoryGirl.define do
  factory :order do |f|
    customer_id       { 1 }
    final_product_id  { 1 }
    month_id          { Faker::Number.between(1, 6)}
    quantity          { Faker::Number.number(2) }
  end
end