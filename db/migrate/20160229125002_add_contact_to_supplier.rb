class AddContactToSupplier < ActiveRecord::Migration
  def change
    add_column :suppliers, :contact_firstname, :string
    add_column :suppliers, :contact_lastname, :string
  end
end
