class CreateShipments < ActiveRecord::Migration
  def change
    create_table :shipments do |t|
      t.integer :supplier_id
      t.integer :rawoil_id
      t.integer :month_id
      t.float :quantity
      t.float :price

      t.timestamps null: false
    end
  end
end
