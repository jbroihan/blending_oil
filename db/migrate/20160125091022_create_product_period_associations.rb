class CreateProductPeriodAssociations < ActiveRecord::Migration
  def change
    create_table :product_period_associations do |t|
      t.integer :product_id
      t.integer :period_id
      t.float :demand
      t.float :production_quantity
      t.float :inventory_level
      t.integer :setup_variable

      t.timestamps null: false
    end
  end
end
