class CreateProductions < ActiveRecord::Migration
  def change
    create_table :productions do |t|
      t.integer :month_id
      t.float :production

      t.timestamps null: false
    end
  end
end
