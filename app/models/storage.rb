class Storage < ActiveRecord::Base
  validates :capacity, presence: true, numericality: { greater_than_or_equal_to: 0 }

  has_many :warehousings, :dependent => :destroy
  has_many :months, :through => :warehousings
  belongs_to :rawoil

  # Returns the ID of the next new storage
  def next_id
    Storage.maximum(:id).next.to_s
  end

  # Returns the storage capacity
  def storage_capacity_param
    Administration.find(3).value
  end

  # Returns the last rawoil id
  def last_rawoil
    Rawoil.maximum(:id)
  end
end
