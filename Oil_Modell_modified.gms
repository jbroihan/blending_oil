$Title Modified Food Manufacturing Problem - Blending of Oils (FOOD,SEQ=352)

Sets m                     planning period
     p                     raw oils
     pv(p)                 vegetable oils
     pnv(p)                non-vegetable oils
     s                     supplier
     c                     customer
     mpa                   rawoil-month-association
     mpoa                  rawoil-month-use-association
     mpsa                  rawoil-month-supplier-association
     mproda                month-production-association
     MP(m,p)               rawoil-month-relation
     MPS(m,p,s)            rawoil-month-supplier-relation
     MPO(m,p)              rawoil-month-use-relation
     MPROD(m)              month-production-relation
     MPaMP(mpa,m,p)
     MPOaMPO(mpoa,m,p)
     MPSaMPS(mpsa,m,p,s)
     MPRODaMPROD(mproda,m);

Parameters
     maxstore        maximum storage of each type of raw oil
     maxusepv        maximum use of vegetable oils
     maxusepnv       maximum use of non-vegetable oils
     minusep         minimum use of raw oil
     maxnusep        maximum number of raw oils in a blend
     sp              sales price of refined and blended oil
     sc              storage cost of raw oils
     stock(p)        stock at the beginning and end
     hmin            minimum hardness of refined oil
     hmax            maximum hardness of refined oil
     h(p)            hardness of raw oils
     cost(m,p,s)     raw oil cost
     demand(m,c)     demand for final product per month and customer;

Variables
     produce(m)      production of blended and refined oil per month
     use(m,p)        usage of raw oil per month
     induse(m,p)     indicator for usage of raw oil per month
     buy(m,p,s)      purchase of raw oil per month from supplier s
     store(m,p)      storage of raw oil at end of the month
     profit          objective variable;

Positive variables produce, buy, store, use;
Binary variable induse;

$include Oil_include.inc

Equations
     defobj          objective
     defusepv(m)     maximum use of vegetable oils
     defusepnv(m)    maximum use of non-vegetable oils
     defproduce(m)   production of refined oil
     demandeq(m)     demand for final products
     defhmin(m)      minmum hardness requirement
     defhmax(m)      maximum hardness requirement
     stockbal(m,p)   stock balance constraint
     minuse(m,p)     minimum usage of raw oil
     maxuse(m,p)     usage of raw oil is 0 if induse is 0
     maxnuse(m)      maximum number of raw oils used in a blend
     deflogic1(m)    if some vegetable raw oil is use we also need to use o1;

defobj..        profit =e=   sum(m, sp*produce(m))
                           - sum((m,p,s), cost(m,p,s)*buy(m,p,s))
                           - sum((m,p), sc*store(m,p));

defusepv(m)..   sum(pv, use(m,pv)) =l= maxusepv;

defusepnv(m)..  sum(pnv, use(m,pnv)) =l= maxusepnv;

defproduce(m).. sum(p, use(m,p)) =e= produce(m);

demandeq(m)..   sum(c, demand(m,c)) =g= produce(m);

defhmin(m)..    sum(p, h(p)*use(m,p)) =g= hmin*produce(m);

defhmax(m)..    sum(p, h(p)*use(m,p)) =l= hmax*produce(m);

* steady-state stock
stockbal(m,p).. sum(s, buy(m,p,s)) + store(m--1,p) - use(m,p) =e= store(m,p);

* Now come the logical constraints
minuse(m,p)..   use(m,p) =g= minusep*induse(m,p);

maxuse(m,p)..   use(m,p) =l= (maxusepv$pv(p)+maxusepnv$pnv(p))*induse(m,p);

maxnuse(m)..    sum(p, induse(m,p)) =l= maxnusep;

* sum(pv, induse(m,pv))>=1 => induse(m,'o3')=1
* turn around induse(m,'o3')=0 => sum(pv, induse(m,pw))=0
deflogic1(m)..  sum(pv, induse(m,pv)) =l= induse(m,'p5')*card(pv);

model food / all /;

store.up(m,p)    = maxstore;
store.fx('m6',p) = stock(p);

option optcr=0;
solve food max profit using mip;

display profit.l, store.l, buy.l, induse.l, use.l, produce.l;

file outputfile1 / 'Blending_of_Oil_solution_store.txt'/;
put outputfile1;
loop(m,
     loop(p,
         loop(mpa$MPaMP(mpa,m,p),
             put mpa.tl:0, ' ; ' m.tl:0, ' ; ' p.tl:0, ' ; ' store.l(m,p) /
         );
     );
);
putclose outputfile1;

file outputfile2 / 'Blending_of_Oil_solution_buy.txt'/;
put outputfile2;
loop(m,
     loop(p,
         loop(s,
             loop(mpsa$MPSaMPS(mpsa,m,p,s),
                 put mpsa.tl:0, ' ; ' m.tl:0, ' ; ' p.tl:0, ' ; '
                     s.tl:0, ' ; ' buy.l(m,p,s) /
                 );
             );
         );
     );
putclose outputfile2;

file outputfile3 / 'Blending_of_Oil_solution_use.txt'/;
put outputfile3;
loop(m,
     loop(p,
         loop(mpoa$MPOaMPO(mpoa,m,p),
             put mpoa.tl:0, ' ; ' m.tl:0, ' ; ' p.tl:0, ' ; '
                 use.l(m,p), ' ; ' induse.l(m,p) /
             );
         );
     );
putclose outputfile3;

file outputfile4 / 'Blending_of_Oil_solution_produce.txt'/;
put outputfile4;
loop(m,
    loop(mproda$MPRODaMPROD(mproda,m),
        put mproda.tl:0, ' ; ' m.tl:0, ' ; ' produce.l(m)/
        );
    );
putclose outputfile4;

file outputfile5 / 'Blending_of_Oil_solution_profit.txt'/;
put outputfile5;
put 'Zielfunktionswert:  'profit.l, '  Model-Status:  'food.modelstat/;
put '**************************************************************'
putclose outputfile5;
