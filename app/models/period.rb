class Period < ActiveRecord::Base
  has_many :products, through: :product_period_associations
  has_many :machines, through: :machine_period_associations
  has_many :product_period_associations, dependent: :destroy
  has_many :machine_period_associations, dependent: :destroy

end
