# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('#productions').dataTable({
    "language": {
      "sSearch": "Suche:",
    }
    "dom": '<"top"i>rt<"bottom"flp><"clear">',
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false,
    "columnDefs": [{ 'type': 'date', 'targets': 0}]
  })

