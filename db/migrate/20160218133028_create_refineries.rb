class CreateRefineries < ActiveRecord::Migration
  def change
    create_table :refineries do |t|
      t.string :name
      t.float :capacity
      t.string :category

      t.timestamps null: false
    end
  end
end
