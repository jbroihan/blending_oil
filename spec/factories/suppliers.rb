require 'faker'

FactoryGirl.define do
  factory :supplier do
    contact_firstname   { Faker::Name.first_name }
    contact_lastname    { Faker::Name.last_name }
    email               { Faker::Internet.email }
    name                { Faker::Company.name }
    gams_id             { 's' + Faker::Number.number(1) }
  end
end