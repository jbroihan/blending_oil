class Supplier < ActiveRecord::Base
  validates :name, :contact_firstname, :contact_lastname, :email, presence: true
  validates_associated :shipments
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }

  has_many :shipments, :dependent => :destroy
  has_many :rawoils, :through => :shipments
  has_many :months, :through => :shipments

  accepts_nested_attributes_for :shipments, allow_destroy: true

  # Returns full name of supplier contact
  def fullname
    "#{contact_firstname} #{contact_lastname}"
  end
end
