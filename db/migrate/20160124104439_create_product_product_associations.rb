class CreateProductProductAssociations < ActiveRecord::Migration
  def change
    create_table :product_product_associations do |t|
      t.integer :predecessor_id
      t.integer :successor_id
      t.float :coefficient

      t.timestamps null: false
    end
  end
end
