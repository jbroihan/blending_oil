require 'faker'

FactoryGirl.define do
  factory :finalProduct do
    name  { Faker::Commerce.product_name }
    price { Faker::Commerce.price }
  end
end