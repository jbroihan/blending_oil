class Product < ActiveRecord::Base
  belongs_to :machine
  has_many :product_product_associations, foreign_key: "successor_id", :dependent => :destroy
  has_many :reverse_product_associations, foreign_key: "predecessor_id", class_name: "ProductProductAssociation", :dependent => :destroy
  has_many :successors, through: :product_associations, source: :successor_id
  has_many :predecessors, through: :reverse_product_associations, source: :predecessor_id
  has_many :periods, through: :product_period_associations
  has_many :product_period_associations, :dependent => :destroy
end


