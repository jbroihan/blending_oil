class Refinery < ActiveRecord::Base
  validates :capacity, presence: true, numericality: {:greater_than_or_equal_to => 0 }

  has_many :rawoils
end
