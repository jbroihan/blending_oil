json.extract! @product_period_association, :id, :product_id, :period_id, :demand, :production_quantity, :inventory_level, :setup_variable, :created_at, :updated_at
