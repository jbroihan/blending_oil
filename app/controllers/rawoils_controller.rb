class RawoilsController < ApplicationController
  load_and_authorize_resource
  before_action :set_rawoil, only: [:show, :edit, :update, :destroy]

  # GET /rawoils
  # GET /rawoils.json
  def index
    @rawoils = Rawoil.all

    respond_to do |format|
      format.html
      format.xls { send_data(@rawoils.to_xls) }
      format.xls {
        filename = "Rawoils-#{Time.now.strftime("%Y%m%d%H%M%S")}.xls"
        send_data(@rawoils.to_xls, :type => "text/xls; charset=utf-8; header=present", :filename => filename)
      }
    end
  end

  # GET /rawoils/1
  # GET /rawoils/1.json
  def show
  end

  # GET /rawoils/new
  def new
    @rawoil = Rawoil.new
  end

  # GET /rawoils/1/edit
  def edit
  end

  # POST /rawoils
  # POST /rawoils.json
  def create
    @rawoil = Rawoil.new(rawoil_params)

    respond_to do |format|
      if @rawoil.save
        format.html { redirect_to new_storage_path, notice: 'Neues Rohöl wurde erfolgreich angelegt. Nun ist das Anlegen eines neuen Lagertanks erforderlich.' }
        format.json { render :show, status: :created, location: @rawoil }
        if @rawoil.category == 'PV'
          @rawoil.refinery_id = 1
          @rawoil.save
        elsif
          @rawoil.category == 'PNV'
          @rawoil.refinery_id = 2
          @rawoil.save
        end
      else
        format.html { render :new }
        format.json { render json: @rawoil.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rawoils/1
  # PATCH/PUT /rawoils/1.json
  def update
    respond_to do |format|
      if @rawoil.update(rawoil_params)
        format.html { redirect_to @rawoil, notice: 'Rohöl wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @rawoil }
      else
        format.html { render :edit }
        format.json { render json: @rawoil.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rawoils/1
  # DELETE /rawoils/1.json
  def destroy
    @rawoil.destroy
    respond_to do |format|
      format.html { redirect_to rawoils_url, notice: 'Rohöl und Lagertank wurden erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_rawoil
    @rawoil = Rawoil.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def rawoil_params
    params.require(:rawoil).permit(:name, :density, :category, :refinery_id, :final_product_id, :supplier_id,
                                   shipments_attributes: [:id, :_destroy, :supplier_id, :rawoil_id, :month_id, :quantity, :price])
  end
end
